<?
 class Commons
{
	private $dateFormatDatabase = 'Y-m-d';
	private $dateTimeFormatDatabase = 'Y-m-d h:i:s';

	private $dateFormatView = 'd/m/Y';
	private $dateTimeFormatView = 'd/m/Y h:i:s';

	private $conn; // Conection instance

	function __get($campo)
	 {
		 return $this->$campo;
	 }

	function __set($campo,$valor)
	 {
		$this->$campo = $valor;
		return true;
	 }


	function convert2View($prop,$value)
	{
		if(eregi('date',$prop) > 0 )
	 	{
	 		return ($this->convertDateView($value,$this->dateFormatDatabase) == '//')?'':$this->convertDateView($value,$this->dateFormatDatabase);
	 	}
	 	elseif(eregi('dtime',$prop) > 0 )
	 	{
	 		return $this->convertDateView($value,$this->dateTimeFormatDatabase,'time');
	 	}
	 	elseif(eregi('money',$prop) > 0 )
	 	{
	 		return $this->convertMoneyView($value);
	 	}
	 	else
	 	{
	 		return $value;
	 	}
	}

	function convert2Save($prop,$value)
	{
		if(eregi('date',$prop) > 0 )
		{
			return ($this->convertDateDataBase($value,$this->dateFormatView) == '//')?'':$this->convertDateDataBase($value,$this->dateFormatView);
		}
		elseif(eregi('dtime',$prop) > 0 )
		{
			return $this->convertDateDataBase($value,$this->dateTimeFormatView,'time');
		}
	 	elseif(eregi('money',$prop) > 0 )
	 	{
			return $this->convertMoneyDataBase($value);
	 	}
	 	elseif(eregi('pass',$prop) > 0 )
	 	{
			return md5($value);
	 	}
	 	else
	 	{
			return $value;
	 	}
	}

	function populateObject($class,$id,$key)
	 {
		$this->conn->select("SELECT * FROM $class WHERE $key = '$id'");
		foreach(get_class_vars($class) as $prop=>$value)
		 {
			$this->$prop = $this->convert2View($prop,$this->conn->getLine($prop));
		 }
	 }

	function __save($class,$campos,$key)
	 {
	 	$className = get_class($class);
	 	foreach(get_class_vars($className) as $prop=>$value)
		 {
		 	if(isset($campos[$prop]) && $prop != $key)
		 	{
		 		$camps[] = $prop;
		 		$val[] = $this->convert2Save($prop,$campos[$prop]);
		 	}
		 }

		  if($this->$key == NULL)
		  {
		  /* AUTO INCREMENT */
			 $sql = "SELECT  MAX($key) as LastId FROM $className";
			 $this->conn->select($sql);
			 $LastId = $this->conn->getLine('LastId') ==  NULL ? '0' : $this->conn->getLine('LastId');
			 $LastId++;
			 $camps[] = $key;
			 $val[] = $LastId;
		  }
		 /* AUTO INCREMENT */
		 /* Return false in case you send only fields that dont't  have in database */
		 if (count($val) <= 0 )
		  {
		  	 return false;
		  }
		 /* End Return false */
		 $str_campos = '';
		 $str_valores = '';
		 for ($i=0;$i!=count($camps) ; $i++)
			  {
			    $str_campos .= "".$camps[$i].",";
				$str_valores.= "'".(addslashes($val[$i]))."',";
			  }
			 $str_campos = substr($str_campos,0,strlen($str_campos)-1);
			 $str_valores= substr($str_valores,0,strlen($str_valores)-1);
	 	 if($this->$key == NULL)
		  {
			 $sql = "INSERT INTO $className($str_campos) VALUES ($str_valores)";
			return $this->conn->insert($sql);
		  }
		  else
		  {
		    $sql = "UPDATE `$className` SET ";
			for($i=0;$i!=count($camps);$i++)
			 {
			  $sql .= $camps[$i]." = '".(addslashes($val[$i]))."',";
			 }
			 $sql = substr($sql,0,strlen($sql)-1);
			 $sql.= " WHERE $key = ".$this->$key;
			 return $this->conn->update($sql);
		  }
	 }

	function __delete($class,$value,$key)
	{
		$className = get_class($class);
		$sql = "DELETE FROM $className WHERE $key = '$value'";
		return $this->conn->delete($sql) ;
	}

	/* Date Conversion to Show on Screen */
	function convertDateView($date,$format,$time=null)
	{

		$replaces = split("[./ : -]",$format);
		$values=split("[./ : -]",$date);
		if($time == null)
		{
		return str_replace($replaces,$values,$this->dateFormatView);
		}
		else
		{
		 return str_replace($replaces,$values,$this->dateTimeFormatView);
		}
	}

	/* Date Conversion to Insert or Update on DataBase */

	function convertDateDataBase($date,$format,$time=null)
	{
		$replaces = split("[/ :]",$format);
		$values=split("[/ :]",$date);
		if($time == null)
		{
		return  str_replace($replaces,$values,$this->dateFormatDatabase);
		}
		else
		{
		return  str_replace($replaces,$values,$this->dateTimeFormatDatabase);
		}
	}

	/* Money Format Conversion to Show on Screen */

	function convertMoneyView($float)
	 {

	 	$Money = number_format($float,2,',','.');
	 	return $Money;
	 }

	/* Money Format to Insert or Update on DataBase */
	function convertMoneyDataBase($float)
	 {
	 	$Money = str_replace('.','',$float);
	 	$Money = str_replace(',','.',$Money);
	 	return $Money;
	 }


	function __listAll($class,$filter="",$order="")
	{
		if(isset($filter) && $filter!=""){
			$filter = " WHERE ".$filter;
		}
		if(isset($order) && $order!=""){
			$order = " ORDER BY ".$order." ASC ";
		}
		$className = get_class($class);
		$this->conn->select("Show fields from $className");
		for($i=0;$i!=$this->conn->totalregistros();$i++)
		{
			$fields[] = $this->conn->pegalinha(0);
			$this->conn->proxima();
		}
		$this->conn->select("Select * from $className ".$filter." ".$order);
		for($i=0,$arrLista=NULL;$i!=$this->conn->totalregistros();$i++)
		{
			foreach($fields as $campos)
			{
				$arrLista[$i][$campos] = $this->convert2View($campos,$this->conn->pegalinha($campos));
			}
			$this->conn->proxima();
		}
		return $arrLista;
	}


	function checkRequireds($campos,$requireds)
    {
	    foreach($campos as $ind=>$valores)
		{
			if (in_array($ind,$requireds))
			{
				if (empty($valores))
				{
					return $ind;
					break;
				}
			}
		}
	}

	function __listSingle($class,$key,$order ='',$filter="")
	{
		if(isset($filter) && $filter!=""){
			$filter = " WHERE ".$filter;
		}
		if(isset($order) && $order!=""){
			$order = " ORDER BY ".$order." ASC ";
		}
		$className = get_class($class);
		$this->conn->select("Select $key from ".$this->conn->__get('database').".$className $filter $order ");
		for($i=0,$ArrLista=NULL;$i!=$this->conn->totalregistros();$i++)
		{
			$ArrLista[] = $this->convert2View($key,$this->conn->pegalinha($key));
			$this->conn->proxima();
		}
		return $ArrLista;
	}

	function dateDiff($dateIni,$dateEnd)
	{
		$di1 = strtotime($dateIni);
 		$di = mktime(0,0,0,date('m',$di1),date('d',$di1),date('Y',$di1));
 		$df1 = strtotime($dateEnd);
 		$df = mktime(0,0,0,date('m',$df1),date('d',$df1),date('Y',$df1));
 		$dias_totais = floor(($df-$di)/86400);
 		return $dias_totais+1;
	}
 }
?>