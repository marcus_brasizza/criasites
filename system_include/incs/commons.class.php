<?
 class Commons
{
	private $dateFormatDatabase = 'Y-m-d';
	private $dateFormatView = 'd/m/Y';


	function __get($campo)
	 {
		 return $this->$campo;
	 }

	function __set($campo,$valor)
	 {
		$this->$campo = $valor;
		return true;
	 }

	function PopulateObject($class,$conn,$id,$key)
	 {
		$conn->select("SELECT * FROM $class WHERE $key = '$id'");
		foreach(get_class_vars($class) as $prop=>$value)
		 {
		 	if(eregi('date',$prop) > 0 )
		 	{
		 	$this->$prop = $this->ConvertDateView($conn->pegalinha($prop),$this->dateFormatDatabase);
		 	}
		 	else
		 	{
		 	 $this->$prop = $conn->pegalinha($prop);
		 	}
		 }
	 }

	function Save($conn,$campos,$class,$key)
	 {
	 	$className = get_class($class);
	 	foreach(get_class_vars($className) as $prop=>$value)
		 {
		 	if(!empty($campos[$prop]))
		 	{
		 		if(eregi('date',$prop) > 0 )
			 	{
			 	$camps[] = $prop;
				$val[] = $this->ConvertDateDataBase($campos[$prop],$this->dateFormatView);
			 	}
			 	elseif(eregi('money',$prop) > 0 )
			 	{
			 	$camps[] = $prop;
				$val[] = $this->ConvertMoneyDataBase($campos[$prop]);
			 	}
			 	else
			 	{
			 	$camps[] = $prop;
				$val[] = $campos[$prop];
			 	}
		 	}
		 }

		  if($this->$key == NULL)
		  {
			  /* AUTO INCREMENT */
			 $sql = "SELECT  MAX($key) as LastId FROM $className";
			 $conn->select($sql);
			 $LastId = $conn->pegalinha('LastId') ==  NULL ? '0' : $conn->pegalinha('LastId');
			 $LastId++;
			 $camps[] = $key;
			 $val[] = $LastId;
		  }
		 /* AUTO INCREMENT */
		 /* Return false in case you send only fields that dont't  have in database */
		 if (count($val) <= 0 )
		  {
		  	 return false;
		  }
		 /* End Return false */
		 $str_campos = '';
		 $str_valores = '';
		 for ($i=0;$i!=count($camps) ; $i++)
			  {
			    $str_campos .= "".$camps[$i].",";
				$str_valores.= "'".(addslashes($val[$i]))."',";
			  }
			 $str_campos = substr($str_campos,0,strlen($str_campos)-1);
			 $str_valores= substr($str_valores,0,strlen($str_valores)-1);
	 	 if($this->$key == NULL)
		  {
			 $sql = "INSERT INTO $className($str_campos) VALUES ($str_valores)";
			return $conn->insert($sql);


		  }
		  else
		  {
		    $sql = "UPDATE `$className` SET ";
			for($i=0;$i!=count($camps);$i++)
			 {
			  $sql .= $camps[$i]." = '".(addslashes($val[$i]))."',";
			 }
			 $sql = substr($sql,0,strlen($sql)-1);
			 $sql.= " WHERE $key = ".$this->$key;
			 echo $sql ;
			 return $conn->update($sql);
		  }
	 }

	function Delete($conn,$value,$class,$key)
	{
		$className = get_class($class);
		$sql = "DELETE FROM $className WHERE $key = '$value'";
		return $conn->delete($sql) ;
	}

	/* Date Conversion do Show on Screen */
	function ConvertDateView($date,$format)
	{

		$replaces = split("[./-]",$format);
		$values=split("[./-]",$date);
		return str_replace($replaces,$values,$this->dateFormatView);
	}

	/* Date Conversion do Insert or Update on DataBase */

	function ConvertDateDataBase($date,$format)
	{

		$replaces = split("[./-]",$format);
		$values=split("[./-]",$date);
		return str_replace($replaces,$values,$this->dateFormatDatabase);

	}

	/* Money Format Conversion do Show on Screen */

	function ConvertMoneyView($float)
	 {
	 	$Money = number_format($float,2,'.',',');
	 	return $Money;
	 }

	/* Money Format do Insert or Update on DataBase */
	function ConvertMoneyDataBase($float)
	 {
	 	$Money = str_replace('.','',$float);
	 	$Money = str_replace(',','.',$Money);
	 	echo $Money;
	 	return $Money;
	 }
	 
	 
	function listAll($conn,$class)
	{
		$className = get_class($class);
		$conn->select("Show fields from $className");
		for($i=0;$i!=$conn->totalregistros();$i++)
		{
			$fields[] = $conn->pegalinha(0);
			$conn->proxima();
		}
		$conn->select("Select *  from $className ");
		for($i=0;$i!=$conn->totalregistros();$i++)
		{
			foreach($fields as $campos)
			{
				$arrLista[$i][$campos] = $conn->pegalinha($campos);
			}
			$conn->proxima();
		}
		return $arrLista;
	}	

 }
?>