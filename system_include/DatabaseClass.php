<?php
// *********************************************************************************
// * Script                  : Generic database connection
// * Programador             : Marcus Vinicius Brasizza
// * Linguagem               : PHP
// * Objetivo                : Layer to connect to any databases (Mysql,Mssql,Interbase/Firebird and PostGrees
// * Observations	         : This Layer is free, Use to whateaver you want, but PLEASE keep this comments
// * If you see any errors please send me an E-mail for (mvbdesenvolvimento(at)gmail.com) and i'll be glat do answer
// *********************************************************************************


abstract class CommonClass
{
	protected $indexDebug=0;
	protected $conn = '';
	protected $dataBase = '';
	protected $indexAshowDebug = 0 ;
	protected $DebugStr = Array();
	protected $DebugAmostra = Array();
	// Methods \\
	function __set($field,$valor)
	{
		$this->$field = $valor;
		return true;
	}

	/* ********************************************************************************************************* */

	function __get($field)
	{
		return $this->$field;
	}

	/* ********************************************************************************************************* */

	public function set_debug($content)
	{
		//$this->DebugStr[$this->indiceDebug] = $content;
		//$this->indiceDebug =  $this->indiceDebug+1;

	}

	/* ********************************************************************************************************* */

	public function set_amostra($content)
	{
		//$this->DebugAmostra[$this->indiceDebug-1] = $content;

	}


	/* ********************************************************************************************************* */

	public function showDebug($decision)
	{
		/*$debug = NULL;

		if(count($this->DebugStr) > 0 )
		{
			$debug= '<table width="100%" cellpadding="1" cellspacing="1" border="0" style="text-align:center ; border: 1px solid #000000;">
				<tr>
				<td style="background-color:#DDEFD1;text-align:center; border: 1px solid #000000"><strong>Sistema de Debug Banco de dados </strong></td>
				</tr>'.chr(13);
			foreach ($this->DebugStr as $index=>$contents)
			{
				$debug.='<tr>';
				$debug.="<td style=\" border: 1px solid #000000; \">$contents</td>";
				$debug.='</tr>';
				if(isset($this->DebugAmostra[$index]))
				{
					$debug.='<tr>';
					$debug.="<td style=\"text-align:center ;  border: 1px solid #000000;background-color:#E1DDC1\"><strong>Amostra de dados do Select</strong></td>";
					$debug.='</tr>';
					$debug.='<tr>';
					$debug.="<td>".$this->DebugAmostra[$index]."</td>";
					$debug.='</tr>';
				}
			}
		}
		if($decision)
		{
			echo $debug;
		}*/
	}

	/* ********************************************************************************************************* */
	public function numRecords()
	{
		return count($this->result)	;
	}

	/* ********************************************************************************************************* */

	public function next()
	{
		$this->line = $this->line + 1;
	}

	/* ********************************************************************************************************* */

	public function prior()
	{
		$this->line = $this->line - 1;
		if($this->line < 0)
		{
			$this->line = 0 ;
		}
	}

	/* ********************************************************************************************************* */
	public function first()
	{
		$this->line = 0 ;
	}

	/* ********************************************************************************************************* */

	public function last()
	{
		$this->line = count($this->result)-1;
	}

	/* ********************************************************************************************************* */

	public function getLine($field)
	{
		return $this->result[$this->line][$field];
	}

	/* ********************************************************************************************************* */
	public function numFields()
	{
		return $this->numFields-1;
	}


}

/* ################################################ */

Interface IBanco
{
	public function connect();
	public function select($query);
	public function insert($query);
	public function update($query);
	public function delete($query);
}

/* ################################################ */
/* Classe Singleton + Factory */

class Connection
{
	public static $MysqlInstance;
	public static $InterbaseInstance;
	public static $SqlServerInstance;
	public static $PostGrInstance;
	public function __construct()
	{
		echo __CLASS__.'  - Não deve ser Instanciada diretamente';
	}

	public static function UseDatabase($tipo,$host,$usuario,$senha,$banco,$porta,$singConnect=true)
	{
	
		switch($tipo)
		{
			case 'mysql':
			
				if($singConnect == false)
				{
					self::$MysqlInstance =  new mysqlconect($host,$usuario,$senha,$banco,$porta);
					return self::$MysqlInstance;
					break;
				}
				else
				{
					if(!isset(self::$MysqlInstance) )
					{
						self::$MysqlInstance =  new mysqlconect($host,$usuario,$senha,$banco,$porta);
						return self::$MysqlInstance;
						break;
					}
				}				
				
				
				break;

			case 'ibase':
				if($singConnect == false)
				{
					self::$InterbaseInstance =  new ibaseconect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				elseif(!isset(self::$InterbaseInstance) )
				{
					self::$InterbaseInstance =  new ibaseconect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				return self::$InterbaseInstance;
				break;

			case 'mssql':
				if($singConnect == false)
				{
					self::$SqlServerInstance =  new mssqlconnect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				elseif(!isset(self::$SqlServerInstance))
				{

					self::$SqlServerInstance =  new mssqlconnect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				return self::$SqlServerInstance;
				break;

			case 'pgsql':
				if($singConnect == false)
				{
					self::$PostGrInstance =  new pgconnect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				elseif(!isset(self::$PostGrInstance))
				{

					self::$PostGrInstance =  new pgconnect($host,$usuario,$senha,$banco,$porta);
					break;
				}
				return self::$PostGrInstance;
				break;

		}

	}

}


/* ################################################ */



/* connection MYSQL */


/* ################################################ */

class mysqlconect extends CommonClass implements IBanco
{
	protected $resultado = Array();
	protected $line = 0 ;
	protected $numFields;
	public function __construct($host,$usuario,$senha,$banco,$porta)
	{


		$this->__set('host',$host);
		$this->__set('usuario',$usuario);
		$this->__set('senha',$senha);
		$this->__set('banco',$banco);
		$this->__set('porta',$porta);

		$this->connect();

	}

	/* ********************************************************************************************************* */

	public function connect()
	{
		$conn = mysqli_connect($this->__get('host').":".$this->__get('porta'),$this->__get('usuario'),$this->__get('senha'));
		$bd = mysqli_select_db($this->__get('banco'),$conn) ;
		$this->connection = $conn;
		$this->database = $this->__get('banco');
		if ((!$conn ) or (!$bd))
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug( "Erro ao conectar  em :  ".__CLASS__."  -  <strong>".mysqli_error($this->connection)."<br>".$e->getMessage()."</strong>" );
			}
		}
		else
		{
			$this->set_debug('Conectado ao mysql em <strong>'.$this->host.'</strong> com o usuario <strong>'.$this->usuario.'</strong> e senha <strong>xxxxxxxx</strong>');
			$this->set_debug('Conectando ao banco de dados <strong>'.$this->banco.'</strong>');
		}

		return $conn;
	}

	/* ********************************************************************************************************* */

	public function select($query)
	{
		@mysqli_select_db($this->__get('database'),$this->__get('connection'));
		$this->result = NULL;
		$i = 0 ;
		$exec = @mysqli_query($query,$this->connection) ;
		if(!$exec)
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) .' - :'.$query."<br>".$e->getMessage()." </strong>");
			}

			return false;
		}
		$fields =  mysqli_num_fields($exec);
		$this->set_debug("A tabela tem <strong>$fields</strong> campos");
		$this->set_debug("Query '<strong>$query</strong>' execudada com sucesso ");
		$this->numFields = $fields;
		$amostra = '<table width=100% border="0"> <tr>';
		for ($j = 0 ; $j != $fields ; $j++)
		{
			$field_str = mysqli_fieldname($exec,$j) ;
			$amostra .= '<td style="background-color:#DDEFD1;text-align:center; border: 1px solid #000000;">'.ucfirst($field_str).'</td>'.chr(13);
		}
		$amostra.='</tr><tr>'.chr(13);
		while ($contents = mysqli_fetch_array($exec))
		{


			for ($j = 0 ; $j != $fields ; $j++)
			{

				$field_str = mysqli_fieldname($exec,$j) ;
				$this->result[$i][$field_str] = $contents[$j];
				$this->result[$i][$j] = $contents[$j];
				if($i <= 5 )
				{
					if($j%2 == 0)
					{
						$fundo = '#F4F4F4';
					}
					else
					{
						$fundo = '#FFEEE6';
					}
					$amostra.='<td style="text-align:center; border: 1px solid #000000;background-color:'.$fundo.'">'.$contents[$j].'</td>'.chr(13);
				}
			}
			$i++;
			if($i <= 5 )
			{
				$amostra.= '</tr><tr>'.chr(13);
			}
		}
		$this->first();
		$amostra.='</table>'.chr(13);
		$this->set_amostra($amostra);
		return true;
	}

	/* ********************************************************************************************************* */

	public function update($query)
	{
		@mysqli_select_db($this->__get('database'),$this->__get('connection'));
		if (is_array($query))
		{
			$i= 1 ;
			foreach ($query as $instr)
			{
				$total = count($query);

				$exec = @mysqli_query(($instr),$this->connection) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "</strong>");
					}
					return false;
				}
				$this->set_debug ("Atualiza��o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @mysqli_query($query,$this->connection);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "<br>".$e->getMessage()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug ("Atualiza��o da tabela executada com sucesso : <strong>".$query."</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */

	public function delete($query)
	{
		@mysqli_select_db($this->__get('database'),$this->__get('connection'));
		if (is_array($query))
		{
			$i= 1 ;
			$total = count($query);
			foreach ($query as $instr)
			{
				$exec = @mysqli_query($instr,$this->connection) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "</strong>");
					}
					return false;
				}
				$this->set_debug ("Exclus�o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @mysqli_query($query,$this->connection);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "<br>".$e->getMessage()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug("Exclus�o efetuada com sucesso :<strong> $query</strong>");
			}
		}
		return true;
	}

	public function insert($query)
	{
		@mysqli_select_db($this->__get('database'),$this->__get('connection'));
		if (is_array($query))
		{
			foreach ($query as $instr)
			{
				$exec = @mysqli_query(($instr),$this->connection) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida em ".__CLASS__." ');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "</strong>");
					}
					return false;
				}
				$id[] = mysqli_insert_id($this->connection);
				$this->set_debug("Inser��o de dados em array , retornando id <strong>".mysqli_insert_id()."</strong> - <strong>$instr</strong>");
			}
		}
		else
		{
			$exec = @mysqli_query($query,$this->connection);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv&aacute;lida em ".__CLASS__."  -  <strong>".mysqli_error($this->connection) . "<br>".$e->getMessage()."</strong>");
				}
				return false;
			}
			$id = mysqli_insert_id($this->connection);
			$this->set_debug("Inser&ccedil;&atilde;o de dados executado com sucesso, retornando id <strong>$id</strong> - <strong>$query</strong>");
		}
		return $id;
	}

}

/* ################################################ */



/* connection INTERBASE */


/* ################################################ */


class ibaseconect extends CommonClass implements IBanco
{
	protected $resultado = Array();
	protected $line = 0 ;
	protected $numFields;
	public function __construct($host,$usuario,$senha,$banco,$porta)
	{
		$this->__set('host',$host);
		$this->__set('usuario',$usuario);
		$this->__set('senha',$senha);
		$this->__set('banco',$banco);
		$this->__set('porta',$porta);
		$this->connect();
	}

	/* ********************************************************************************************************* */

	public function connect()
	{
		$conn = @ibase_connect($this->__get('host').$this->__get('banco'),$this->__get('usuario'),$this->__get('senha'));
		$this->connection = $conn;
		$this->database = $this->__get('banco');
		if ((!$conn ) )
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug( "Erro ao conectar  em :  ".__CLASS__."  -  <strong>".ibase_errmsg()."<br>".$e->getMessage()."</strong>" );
			}
		}
		else
		{
			$this->set_debug('Conectado ao Ibase em <strong>'.$this->host.'</strong> com o usuario <strong>'.$this->usuario.'</strong> e senha <strong>xxxxxxxx</strong>');
			$this->set_debug('Conectando ao banco de dados <strong>'.$this->banco.'</strong>');
		}

		return $conn;
	}

	/* ********************************************************************************************************* */

	public function select($query)
	{
		$i = 0 ;
		$j = 0 ;
		$exec = @ibase_query($query) ;
		if(!$exec)
		{
			try
			{
				throw new Exception("Query Inv�lida em ".__CLASS__);
			}
			catch (Exception $e)
			{
				$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_errmsg(). "<br>".$e->getMessage()."</strong>");
			}
			return false;
		}
		$fields =  ibase_num_fields($exec);
		$this->numFields = $fields;$contents = NULL;
		$amostra = '<table width=100% border="0"> <tr>';
		for ($j = 0 ; $j != $fields ; $j++)
		{
			$field_str = ibase_field_info($exec,$j) ;
			echo "<pre>";

			$amostra .= '<td style="background-color:#DDEFD1;text-align:center; border: 1px solid #000000;">'.ucfirst(strtolower($field_str['name'])).'</td>'.chr(13);
		}
		$amostra.='</tr><tr>'.chr(13);
		while ($contents = ibase_fetch_row($exec))
		{
			for ($j = 0 ; $j != $fields ; $j++)
			{
				$camp = ibase_field_info($exec,$j);
				$this->result[$i][strtolower($camp["name"])]	 = $contents[$j] ;
				$this->result[$i][$j] = $contents[$j];
				if($i <= 5 )
				{
					if($j%2 == 0)
					{
						$fundo = '#F4F4F4';
					}
					else
					{
						$fundo = '#FFEEE6';
					}
					$amostra.='<td style="text-align:center; border: 1px solid #000000;background-color:'.$fundo.'">'.$contents[$j].'</td>'.chr(13);
				}
			}
			$i++;
			if($i <= 5 )
			{
				$amostra.= '</tr><tr>'.chr(13);
			}
		}
		$this->first();
		$amostra.='</table>'.chr(13);
		$this->set_amostra($amostra);

		return true;
	}

	/* ********************************************************************************************************* */

	public function update($query)
	{
		if (is_array($query))
		{
			$i= 1 ;
			foreach ($query as $instr)
			{
				$total = count($query);

				$exec = @ibase_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_error() . "</strong>");
					}
					return false;
				}
				$this->set_debug ("Atualiza��o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @ibase_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_error() . "<br>".$e->getMessage()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug ("Atualiza��o da tabela executada com sucesso : <strong>".$query."</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */

	public function delete($query)
	{

		if (is_array($query))
		{
			$i= 1 ;
			$total = count($query);
			foreach ($query as $instr)
			{
				$exec = @ibase_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_error() . "</strong>");
					}
					return false;
				}
				$this->set_debug ("Exclus�o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @ibase_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_errmsg(). "<br>".$e->getMessage()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug("Exclus�o efetuada com sucesso :<strong> $query</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */


	public function insert($query)
	{
		$id = '';
		if (is_array($query))
		{					
			foreach ($query as $instr)
			{
				$exec = @ibase_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida em '.__CLASS__);
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_errmsg(). " : $instr</strong>");
					}
					return false;
				}

				$this->set_debug("Inser��o de dados em array - <strong>$instr</strong>");
			}
		}
		else
		{
			$exec = @ibase_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".ibase_errmsg(). " : $query"."<br>".$e->getMessage()."</strong>");
				}
				return false;
			}

			$this->set_debug("Inser��o de dados executado com sucesso  - <strong>$query</strong>");
		}
		return $id;
	}

}

/* ################################################ */



/* connection SQL SERVER */


/* ################################################ */

class mssqlconnect extends CommonClass implements IBanco
{
	protected $resultado = Array();
	protected $line = 0 ;
	protected $numFields;
	public function __construct($host,$usuario,$senha,$banco,$porta)
	{

		$this->__set('host',$host);
		$this->__set('usuario',$usuario);
		$this->__set('senha',$senha);
		$this->__set('banco',$banco);
		$this->__set('porta',$porta);
		$this->connect();
	}

	/* ********************************************************************************************************* */

	public function connect()
	{

		$conn = mssql_connect($this->__get('host'),$this->__get('usuario'),$this->__get('senha'));

		$bd = mssql_select_db($this->__get('banco')) ;
		$this->connection = $conn;
		$this->database = $this->__get('banco');
		if ((!$conn ) or (!$bd))
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug( "Erro ao conectar  em :  ".__CLASS__."  -  <strong>Erro ao Conectar ao Banco de dados</strong>" );
			}
		}
		else
		{
			$this->set_debug('Conectado ao mysql em <strong>'.$this->host.'</strong> com o usuario <strong>'.$this->usuario.'</strong> e senha <strong>xxxxxxxx</strong>');
			$this->set_debug('Conectando ao banco de dados <strong>'.$this->banco.'</strong>');
		}

		return $conn;
	}

	/* ********************************************************************************************************* */

	public function select($query)
	{
		$this->result = NULL;
		$i = 0 ;
		$j = 0 ;
		$exec = @mssql_query($query) ;

		if(!$exec)
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
			}

			return false;
		}
		$fields =  mssql_num_fields($exec);
		$this->set_debug("A tabela tem <strong>$fields</strong> campos");
		$this->set_debug("Query '<strong>$query</strong>' execudada com sucesso ");
		$this->numFields = $fields;


		$contents = NULL;
		$amostra = '<table width=100% border="0"> <tr>';
		for ($j = 0 ; $j != $fields ; $j++)
		{
			$field_str = mssql_field_name($exec,$j) ;
			$amostra .= '<td style="background-color:#DDEFD1;text-align:center; border: 1px solid #000000;">'.ucfirst($field_str).'</td>'.chr(13);
		}
		$amostra.='</tr><tr>'.chr(13);
		while ($contents = mssql_fetch_array($exec))
		{


			for ($j = 0 ; $j != $fields ; $j++)
			{

				$field_str = mssql_field_name($exec,$j) ;
				$this->result[$i][$field_str] = $contents[$j];
				$this->result[$i][$j] = $contents[$j];
				if($i <= 5 )
				{
					if($j%2 == 0)
					{
						$fundo = '#F4F4F4';
					}
					else
					{
						$fundo = '#FFEEE6';
					}
					$amostra.='<td style="text-align:center; border: 1px solid #000000;">'.$contents[$j].'</td>'.chr(13);
				}
			}
			$i++;
			if($i <= 5 )
			{
				$amostra.= '</tr><tr>'.chr(13);
			}
		}
		$this->first();
		$amostra.='</table>'.chr(13);
		$this->set_amostra($amostra);
		return true;
	}

	/* ********************************************************************************************************* */

	public function update($query)
	{
		if (is_array($query))
		{
			$i= 1 ;
			foreach ($query as $instr)
			{
				$total = count($query);

				$exec = @mssql_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
					}
					return false;
				}
				$this->set_debug ("Atualiza��o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}

		else
		{
			$exec = @mssql_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug ("Atualiza��o da tabela executada com sucesso : <strong>".$query."</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */

	public function delete($query)
	{

		if (is_array($query))
		{
			$i= 1 ;
			$total = count($query);
			foreach ($query as $instr)
			{
				$exec = @mssql_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
					}
					return false;
				}
				$this->set_debug ("Exclus�o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @mssql_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug("Exclus�o efetuada com sucesso :<strong> $query</strong>");
			}
		}
		return true;
	}

	public function insert($query)
	{
		$id = '';
		if (is_array($query))
		{
			$i=0;
			$total = count($query);
			foreach ($query as $instr)
			{
				$exec = @mssql_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida em ".__CLASS__." ');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
					}
					return false;
				}
				$this->set_debug("Inser��o de dados em array </strong> - <strong>$instr</strong>");
			}
		}
		else
		{
			$exec = @mssql_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>: ".$query."</strong>");
				}
				return false;
			}
			$this->set_debug("Inser��o de dados executado com sucesso,  - <strong>$query</strong>");
		}
		return true;
	}

}

/* ################################################ */



/* connection POSTGREE */


/* ################################################ */


class pgconnect extends CommonClass implements IBanco
{
	protected $resultado = Array();
	protected $line = 0 ;
	protected $numFields;
	public function __construct($host,$usuario,$senha,$banco,$porta)
	{
		$this->__set('host',$host);
		$this->__set('usuario',$usuario);
		$this->__set('senha',$senha);
		$this->__set('banco',$banco);
		$this->__set('porta',$porta);
		$this->connect();
	}

	/* ********************************************************************************************************* */

	public function connect()
	{
		$conn = @pg_connect("host=".$this->__get('host')." port=".$this->__get('porta')." dbname=".$this->__get('banco')." user=".$this->__get('usuario')." password=".$this->__get('senha'));

		if ((!$conn ) )
		{
			try
			{
				throw new Exception('Query Inv�lida');
			}
			catch (Exception $e)
			{
				$this->set_debug( "Erro ao conectar  em :  ".__CLASS__."  -  <strong>".pg_result_error()."</strong>" );
			}
		}
		else
		{
			$this->set_debug('Conectado ao PostGree em <strong>'.$this->host.'</strong> com o usuario <strong>'.$this->usuario.'</strong> e senha <strong>xxxxxxxx</strong>');
			$this->set_debug('Conectando ao banco de dados <strong>'.$this->banco.'</strong>');
		}

		return $conn;
	}

	/* ********************************************************************************************************* */

	public function select($query)
	{
		$i = 0 ;
		$j = 0 ;
		$exec = @pg_query($query) ;
		if(!$exec)
		{
			try
			{
				throw new Exception("Query Inv�lida em ".__CLASS__);
			}
			catch (Exception $e)
			{
				$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error(). "</strong>");
			}
			return false;
		}
		$fields =  pg_num_fields($exec);
		$this->numFields = $fields;$contents = NULL;
		$amostra = '<table width=100% border="0"> <tr>';
		for ($j = 0 ; $j != $fields ; $j++)
		{
			$field_str = pg_field_name($exec,$j) ;

			echo "<pre>";
			$amostra .= '<td style="background-color:#DDEFD1;text-align:center; border: 1px solid #000000;">'.ucfirst(strtolower($field_str)).'</td>'.chr(13);
		}
		$amostra.='</tr><tr>'.chr(13);
		while ($contents = pg_fetch_row($exec))
		{
			for ($j = 0 ; $j != $fields ; $j++)
			{
				$camp = pg_field_name($exec,$j);
				$this->result[$i][strtolower($camp)]	 = $contents[$j] ;
				$this->result[$i][$j] = $contents[$j];
				if($i <= 5 )
				{
					if($j%2 == 0)
					{
						$fundo = '#F4F4F4';
					}
					else
					{
						$fundo = '#FFEEE6';
					}
					$amostra.='<td style="text-align:center; border: 1px solid #000000;">'.$contents[$j].'</td>'.chr(13);
				}
			}
			$i++;
			if($i <= 5 )
			{
				$amostra.= '</tr><tr>'.chr(13);
			}
		}
		$this->first();
		$amostra.='</table>'.chr(13);
		$this->set_amostra($amostra);

		return true;
	}

	/* ********************************************************************************************************* */

	public function update($query)
	{
		if (is_array($query))
		{
			$i= 1 ;
			foreach ($query as $instr)
			{
				$total = count($query);

				$exec = @pg_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."</strong>");
					}
					return false;
				}
				$this->set_debug ("Atualiza��o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @pg_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug ("Atualiza��o da tabela executada com sucesso : <strong>".$query."</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */

	public function delete($query)
	{

		if (is_array($query))
		{
			$i= 1 ;
			$total = count($query);
			foreach ($query as $instr)
			{
				$exec = @pg_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida');
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."</strong>");
					}
					return false;
				}
				$this->set_debug ("Exclus�o em array: <strong>$i/$total</strong> - <strong>".$instr."</strong>");
				$i++;
			}
		}
		else
		{
			$exec = @pg_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."</strong>");
				}
				return false;
			}
			else
			{
				$this->set_debug("Exclus�o efetuada com sucesso :<strong> $query</strong>");
			}
		}
		return true;
	}

	/* ********************************************************************************************************* */


	public function insert($query)
	{
		$id = '';
		if (is_array($query))
		{						
			foreach ($query as $instr)
			{
				$exec = @pg_query(($instr)) ;
				if(!$exec)
				{
					try
					{
						throw new Exception('Query Inv�lida em '.__CLASS__);
					}
					catch (Exception $e)
					{
						$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."</strong>");
					}
					return false;
				}

				$this->set_debug("Inser��o de dados em array - <strong>$instr</strong>");
			}
		}
		else
		{
			$exec = @pg_query($query);
			if(!$exec)
			{
				try
				{
					throw new Exception('Query Inv�lida em ".__CLASS__." ');
				}
				catch (Exception $e)
				{
					$this->set_debug("Query Inv�lida em ".__CLASS__."  -  <strong>".pg_result_error()."<br>".$e->getMessage()."</strong>");
				}
				return false;
			}

			$this->set_debug("Inser��o de dados executado com sucesso  - <strong>$query</strong>");
		}
		return $id;
	}

}


?>
