<?php

class System {

    function iniciaProjeto($pasta) {
        if (substr($pasta, -1) == '/') {
            $pasta = substr($pasta, 0, strlen($pasta) - 1);
        }
        if (!@dir($pasta)) {


            @mkdir($pasta);
            $old = umask(0);
            @chmod("$pasta", 0777);
            @umask($old);
        }
        /* Faz a inicializacao da pasta do projeto */
        $old = umask(0);
        /* web */
        
         if (!is_dir($pasta . '/lib')) {
              mkdir($pasta . '/lib', 0777, true);
         }
        
        if (!is_dir($pasta . '/web')) {
            mkdir($pasta . '/web/js', 0777, true);
            mkdir($pasta . '/web/img', 0777, true);
            mkdir($pasta . '/web/css', 0777, true);
        }
        if (!is_dir($pasta . '/doc')) {
            mkdir($pasta . '/doc', 0777, true);
        }
        /* System */

        if (!is_dir($pasta . '/system')) {
            mkdir($pasta . '/system', 0777, true);
        }



        /* Config */
        if (!is_dir($pasta . '/config')) {
            mkdir($pasta . '/config', 0777, true);
        }


        /* View */

        if (!is_dir($pasta . '/app')) {
           mkdir($pasta . '/app/view', 0777, true);
           /* mkdir($pasta . '/app/view/www', 0777, true);
            mkdir($pasta . '/app/view/adm', 0777, true);*/
            mkdir($pasta . '/app/bean', 0777, true);
            mkdir($pasta . '/app/model', 0777, true);
            mkdir($pasta . '/app/controller', 0777, true);
        }




        /* Docs */



        /* @mkdir($pasta.'/class',0777,true); 		
          @mkdir($pasta.'/bean',0777,true);
          @mkdir($pasta.'/includes',0777,true);
          @mkdir($pasta.'/documents',0777,true);
          @
          @mkdir($pasta.'/forms',0777,true); */
        @umask($old);

        return true;
    }

    function listaBancos($conn) {
        $arrLista = array();
        $conn->select("show databases");
        for ($i = 0; $i != $conn->numRecords(); $i++) {
            $arrLista[$conn->getLine(0)] = $conn->getLine(0);
            $conn->next();
        }
        return $arrLista;
    }

    function listaTabelasCombo($conn, $banco) {
        $arrLista = array();
        $conn->select("Show tables");
        for ($i = 0; $i != $conn->numRecords(); $i++) {
            $arrLista[] = $conn->getLine(0);
            $conn->next();
        }
        return $arrLista;
    }

    function listaTabelas($conn, $path) {
        $arrLista = array();
        $conn->select("Show tables");
        for ($i = 0; $i != $conn->numRecords(); $i++) {
            $arrLista[$i]['tabela'] = $conn->getLine(0);
            $x = explode('_', $conn->getLine(0));
            $tbs = '';
            foreach ($x as $t) {
                $tbs.= ucfirst($t);
            }
            if (file_exists($path . '/app/bean/' . $tbs . 'Bean.php')) {
                $arrLista[$i]['class'] = '<span style="color:green;">Yes</span>';
            } else {
                $arrLista[$i]['class'] = '<span style="color:#FF0000;">No</span>';
            }

            if (file_exists($path . '/forms/' . $conn->getLine(0) . '.form.xml')) {
                $arrLista[$i]['form'] = '<span style="color:green;">Yes</span>';
            } else {
                $arrLista[$i]['form'] = '<span style="color:#FF0000;">No</span>';
            }
            $conn->next();
        }
        return $arrLista;
    }

    function criaXmlRelacao($path, $total, $projeto, $bancoSelecionado) {
        if (substr($path, -1) == '/') {
            $path = substr($path, 0, strlen($path) - 1);
        }
        @unlink($path . '/relations/relations_' . $bancoSelecionado . '.xml');
        $handle = fopen($path . '/relations/relations_' . $bancoSelecionado . '.xml', 'a');
        $xmlString = '<?xml version="1.0" encoding="iso-8859-1"?>' . PHP_EOL;
        $xmlString .= '<DATA>' . PHP_EOL;
        for ($i = 0; $i != $total + 1; $i++) {
            if (!empty($_SESSION['relacionamentos'][$i]['tabela1'])) {
                $xmlString.= '<tabela1>';
                $xmlString.= $_SESSION['relacionamentos'][$i]['tabela1'];
                $xmlString.= '</tabela1>' . PHP_EOL;
                $xmlString.= '<tabela2>';
                $xmlString.= $_SESSION['relacionamentos'][$i]['tabela2'];
                $xmlString.= '</tabela2>' . PHP_EOL;
                $xmlString.= '<campo1>';
                $xmlString.= $_SESSION['relacionamentos'][$i]['campo1'];
                $xmlString.= '</campo1>' . PHP_EOL;
                $xmlString.= '<campo2>';
                $xmlString.= $_SESSION['relacionamentos'][$i]['campo2'];
                $xmlString.= '</campo2>' . PHP_EOL . PHP_EOL;
            }
        }
        $xmlString .= '</DATA>' . PHP_EOL;
        fwrite($handle, $xmlString);
        fclose($handle);
        return true;
    }

}

?>