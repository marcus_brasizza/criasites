<?php
class System {

	//propriedades
	private $url;
	private $explode;
	private $folder;
	public $controller;
	public $action;
	public $params;

	public function __construct() {
		$this->setUrl();
		$this->setExplode();
		$this->setFolder();
		$this->setController();
		$this->setAction();
		$this->setParams();
	}

	private function setUrl() {
		$this->url = isset ($_GET['url']) ? $_GET['url'] . '/' : 'index/index_action';
	}

	private function setExplode() {
		$this->explode = explode('/', $this->_url);
	}

	private function setFolder() {
		if($this->explode[0] == 'adm' || $this->explode[0] = 'adm/'){
			$this->folder = 'adm/';
		}
		elseif($this->explode[0] == 'orm' || $this->explode[0] == 'orm/'){
			$this->folder = 'orm/';
		}
		else{
			$this->folder = 'www/';
		}		
	}

	private function setController() {
		if ($this->folder == 'adm/' || $this->folder == 'orm/') {
			$this->controller = $this->explode[1] == null ? 'usuario' : $this->explode[1];
		}else {
			$this->controller = $this->explode[0];
		}
	}

	private function setAction() {
		if ($this->_folder == 'adm/' || $this->_folder == 'orm/') {
			$this->_action = !isset ($this->explode[2]) || $this->explode[2] == null || $this->explode[2] == 'index' ? 'index_action' : $this->explode[2];
		} else {
			$this->_action = !isset ($this->explode[1]) || $this->explode[1] == null || $this->explode[1] == 'index' ? 'index_action' : $this->explode[1];
		}
	}

	private function setParams() {
		if ($this->folder == 'adm/' || $this->folder == 'orm/') {
			unset ($this->explode[0], $this->explode[1], $this->explode[2]);
		} else {
			unset ($this->explode[0], $this->explode[1]);
		}

		if (end($this->explode) == null) {
			array_pop($this->explode);
		}

		$i = 0;
		$ind = array ();
		$value = array ();

		if (!empty ($this->explode)) {
			foreach ($this->explode as $vals) {
				if ($i % 2 == 0) {
					$ind[] = $vals;
				} //endif
				else {
					$value[] = $vals;
				} //endelse
				$i++;
			} //endforeach
		} //endif

		if (count($ind) == count($value) && !empty ($ind) && !empty ($value)) {
			$this->params = array_combine($ind, $value);
		} else {
			$this->params = array ();
		}
	}

	public function getParam($name = null) {
		return $name != null ? $this->params[$name] : $this->params;
	}

	public function run() {
		$controller_path = CONTROLLERS . ucfirst($this->controller) . 'Controller.php';
		if (!file_exists($controller_path)) {
			echo 'Controller <strong>' . ucfirst($this->controller) . '</strong> nao encontrado.';
		} else {
			require_once ($controller_path);
		}

		$app = new $this->_controller;
		$action = $this->_action;

		if (!method_exists($app, $action)) {
			die('O metodo <strong>' . $action . '</strong> nao existe!');
		} else {
			$app-> $action ();

		}
	}

}
?>