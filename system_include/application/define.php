<?php
	
	define('BEAN', 'app/bean/');
	define('CONTROLLER', 'app/controller/');
	define('MODEL', 'app/model/');
	define('VIEW', 'app/view/');
	
	define('CSS', 'web/css/');
	define('JS', 'web/js/');
	define('IMG', 'web/img/');
	
?>
