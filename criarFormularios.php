<?
require("Smarty/Smarty.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");
$tpl = new Smarty();

$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
$hostServer = $_SESSION['system']['host'];
$userServer = $_SESSION['system']['user'];
$passServer = $_SESSION['system']['pass'];
if(!isset($_SESSION['system']))
{
header('location:index.php');
}
$sys = new System();
$projeto = $_SESSION['system']['projeto'];
$path_projeto = $_SESSION['system']['path'].$_SESSION['system']['projeto'];
$bancoSelecionado = $_SESSION['system']['database'];
$tpl->assign('bancoSelecionado',$bancoSelecionado);
$conn = Conexao::UsarBanco($tipoBanco,$hostServer,$userServer,$passServer,$_SESSION['system']['database'],0,false);
$escolha[] = 'Escolha uma Tabela';
$tabelas  = array_merge($escolha,$sys->listaTabelasCombo($conn,$bancoSelecionado));
$tpl->assign('valores_tabelas',$tabelas);
$tpl->display('criarFormularios.html');
?>