<?

require("Smarty/Smarty.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");
require("formMaker/gerador.class.php");
$bancoSelecionado = $_SESSION['system']['database'];
$conn = Conexao::UsarBanco($tipoBanco,$dbServer,$dbUser,$dbPass,$dbDB,0,false);
$system = new System();
if(!session_register('fields'))
{
	session_register('fields');
}

/* Tipos de dados */
$tipos['varchar'] = 'Varchar';
$tipos['int'] = 'Integer';
$tipos['float'] = 'Float';
$tipos['text'] = 'Text';
$tipos['datetime'] = 'DateTime';
/* Fim dos tipos */

if($_POST == true)
{
	if(!empty($_POST['tabela']))
	{
		$_SESSION['system']['tabelaAtual'] = $_POST['tabela'];
	}

	if(!in_array($_POST['campo'],explode('*',$_SESSION['system']['repetidos'])))
	{
		$_SESSION['system']['fields'][count($_SESSION['system']['fields'])]['campo'] = $_POST['campo'];
		$_SESSION['system']['fields'][count($_SESSION['system']['fields'])-1]['tipo'] = $_POST['tipo'];
		if(!empty($_POST['tamanho'])){
			$_SESSION['system']['fields'][count($_SESSION['system']['fields'])-1]['tamanho'] = $_POST['tamanho'];
		}
		$_SESSION['system']['repetidos'].=$_POST['campo'].'*';
	}
}

if(isset($_GET['gerarTabela']))
{
	$sql = "Create Table ". $_SESSION['system']['tabelaAtual']. "(" ;
	foreach($_SESSION['system']['fields'] as $array)
	{
		if(!empty($array['tamanho']))
		{
			$sql.= $array['campo']." ".$array['tipo']."(".$array['tamanho']."),";
		}
		else
		{
			$sql.= $array['campo']." ".$array['tipo'].",";
		}
	}
	$sql = substr($sql,0,strlen($sql)-1).")";
	$hostServer = $_SESSION['system']['host'];
	$userServer = $_SESSION['system']['user'];
	$passServer = $_SESSION['system']['pass'];
	$path_projeto = $_SESSION['system']['path'].$_SESSION['system']['projeto'];
	$connLocadora = Conexao::UsarBanco($tipoBanco,$hostServer,$userServer,$passServer,$_SESSION['system']['database'],0,false);
	if($connLocadora->update($sql))
	{
		unset($_SESSION['system']['tabelaAtual']);
		unset($_SESSION['system']['fields']);
		header('location:listaTabelas.php');
	}
}

$tabelaSelecionada = isset($_SESSION['system']['tabelaAtual'])?$_SESSION['system']['tabelaAtual']:'';
/* Cria o formul�rio de cria��o de tabelas */
$gerador = new form('MyForm1','Preencha os dados abaixo','','post','');
$gerador->SET_inputText('Nome da Tabela','tabela','30',$tabelaSelecionada);
$gerador->SET_inputText('Nome do Campo','campo','30','');
$gerador->SET_select('Tipo de dados','tipo',1,$tipos,'-1','ChecaTipo(this.value,\'tamanho\')');
$gerador->SET_inputNumber('Tamanho do Campo','tamanho','10','');
$gerador->SET_submit('submit','Acrescentar');
$gerador->form_end();
$formulario = $gerador->showForm();
/* Fim da cria��o */
$tpl = new Smarty();

$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
if(isset($_SESSION['system']['fields'])){
	$tpl->assign('fields',$_SESSION['system']['fields']);
	if(count($_SESSION['system']['fields']) > 0 )
	{
		$tpl->assign('mostraTabela','sim');
	}
}
if(isset($_SESSION['system']['tabelaAtual'])){
	$tpl->assign('nomeTabela', $_SESSION['system']['tabelaAtual']);
}
$tpl->assign('areaForm',$formulario);
$tpl->assign('bancoSelecionado',$bancoSelecionado);
$tpl->display('criarTabela.html');


?>