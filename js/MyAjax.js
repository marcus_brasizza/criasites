// +----------------------------------------------------------------------+
// |Javascript OOP                                                        |
// +----------------------------------------------------------------------+
// | Copyright (c) 2008						                              |
// +----------------------------------------------------------------------+
// | Class to access Ajax  Syncronous connection      	  				  |
// | Developed by Marcus Vinicius Brasizza				              	  |
// +----------------------------------------------------------------------+
// | Author: Original Author <mvbdesenvolvimento@gmail.com>              |
// +----------------------------------------------------------------------+
	function Ajax()
	{
// +----------------------------------------------------------------------+
// | Ajax Constructor :										              |
// | Return the instance of Initialize and extends the Prepare Class      |
// | and Initialize Class 											      |
// +----------------------------------------------------------------------+
	Prepare.prototype = new Calls(); // Class Extension
	Initialize.prototype = new Prepare(); // Class Extension
	return new Initialize();
	}

	function Initialize()
	{

		Initialize.obj = false;

// +--------------------------------------------------------+
// Try many Ajax components like ,							|
// IE Component , Firefox Component and Opera Component....	|
// +--------------------------------------------------------+

		Initialize.prototype.objectIDs = new Array("Microsoft.XMLHTTP","Msxml2.XMLHTTP","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP.4.0");
		Initialize.prototype.success = false;

		for (i=0; !this.sucess && i < this.objectIDs.length; i++) {
			try {
				this.obj = new ActiveXObject(this.objectIDs[i]);
				this.success = true;
// +--------------------------------------------------------+
// IF the component was found the sucess assign true		|
// and the system doesn't try again							|
// +--------------------------------------------------------+
			}
			catch (e){
				this.obj = false;
			}
		}
		if (!this.obj){
			this.obj = new XMLHttpRequest();
// +--------------------------------------------------------+
// If the system doesn't found the proper component			|
// It Assign a default component 							|
// +--------------------------------------------------------+

		}
	}


	function Prepare()
	{
// +--------------------------------------------------------+
// This class prepare the ajax's settings to send the		|
// Syncronous connection									|
// +--------------------------------------------------------+
		Prepare.prototype.page = '';
		Prepare.prototype.affect = '';
		Prepare.prototype.callback = '';
		Prepare.prototype.method = '';
		Prepare.prototype.type = '';
		Prepare.prototype.formPage = '';
		/* GETTERS & SETTERS  */
		this.GETpage = function () {
			return this.page;
		}

		this.SETpage = function (valor) {
			this.page = valor;
		}
// +--------------------------------------------------------+
		this.GETaffect = function () {
			return this.affect;
		}

		this.SETaffect = function (valor) {
			this.affect = valor;
		}
// +--------------------------------------------------------+
		this.GETcallback = function () {
			return this.callback;
		}

		this.SETcallback = function (valor) {
			this.callback = valor;
		}
// +--------------------------------------------------------+
		this.GETmethod = function () {
			return this.method;
		}

		this.SETmethod = function (valor) {
			this.method = valor.toUpperCase();
		}
// +--------------------------------------------------------+
		this.GETtype = function () {
			return this.type;
		}

		this.SETtype = function (valor) {
			this.type = valor;
		}
// +--------------------------------------------------------+
		this.GETform = function () {
			return this.formPage;
		}

		this.SETform = function (valor) {
			this.formPage = valor;
		}
// +--------------------------------------------------------+
// If the system doesn't found the proper component			|
// It Assign a default component 							|
// +--------------------------------------------------------+
		function checkform(formbase) {				
			var conteudo = '';			
			 for(i=0; i!= formbase.elements.length ; i++)
			 {
				 
				 
			 	if((formbase.elements[i].type == 'checkbox') || (formbase.elements[i].type == 'radio'))
			 	{
			 	 if (formbase.elements[i].checked == true)
			 	  {
					conteudo += formbase.elements[i].name+'='+encodeURI(formbase.elements[i].value)+'&';
			 	  }
			 	}
			 	else if( (formbase.elements[i].type != 'submit') && (formbase.elements[i].type !='button') && (formbase.elements[i].type !='reset'))
			 	{
					conteudo += formbase.elements[i].name+'='+encodeURI(formbase.elements[i].value)+'&';			
				}
			 }

				return (conteudo.substr(0,conteudo.length-1));
	 	}
// +---------------------------------------------------------+
// This function sends the HTTPRequest choosing POST or GET	 |
// if you choose POST , the system will get the form content |
// or GET will just send the data and receive a querystring	 |
// +---------------------------------------------------------+
		this.Send = function(){
			 lay = document.createElement('divajax');
		     lay.id = 'load';
			 lay.innerHTML = '  <strong>Loading...</strong>';
		     lay.style.position = 'fixed';
			 lay.style.border = 'thin solid #000000';
		     lay.style.top = '3px';
		     lay.style.right = '3px';
		     lay.style.backgroundColor = '#FF0000';
		     lay.style.color = '#FFFFFF';
			 lay.style.width = '120px';
			 lay.style.textAlign = 'center';
			 lay.style.height = '20px';
		     document.body.insertBefore(lay,document.body.firstChild);
			 
		     if(this.GETmethod() == 'POST')
		     {		
					result = checkform(this.GETform());		
				 	this.obj.open('POST',this.GETpage(),false);
				 	this.obj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;")
				 	this.obj.setRequestHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				 	this.obj.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
				 	this.obj.setRequestHeader("Pragma", "no-cache");
					cb = this.GETcallback();
					return eval('this.'+cb+'(\''+result+'\')');
			 }
			 else
			 {
				 	this.obj.open('GET',this.GETpage(),false);
				 	this.obj.setRequestHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				 	this.obj.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
				 	this.obj.setRequestHeader("Pragma", "no-cache");
					cb = this.GETcallback();
					return eval('this.'+cb+'()');
			 }
		}
	}