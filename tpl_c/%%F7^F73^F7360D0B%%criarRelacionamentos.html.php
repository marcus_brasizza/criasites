<?php /* Smarty version 2.6.19, created on 2010-07-20 14:28:52
         compiled from criarRelacionamentos.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'criarRelacionamentos.html', 27, false),array('function', 'html_options', 'criarRelacionamentos.html', 44, false),)), $this); ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- DW6 -->
<head>
<script type="text/javascript" src="js/MyAjax.js"></script>
<script type="text/javascript" src="js/callbacks.js"></script>
<script type="text/javascript" src="js/chamadas.js"></script>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>:: Cria&ccedil;&atilde;o de relacionamentos ::</title>

<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<!-- The structure of this file is exactly the same as 2col_rightNav.html;
     the only difference between the two is the stylesheet they use -->
<body>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "topo.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- end masthead -->
<div id="content">
  <h2 id="pageName">Relationships</h2>
  <div class="story">
    <p>Selected Database <strong>&quot;<?php echo $this->_tpl_vars['bancoSelecionado']; ?>
&quot;</strong></p>
	<?php if ($this->_tpl_vars['listar'] == 'sim'): ?>
	<?php if ($this->_tpl_vars['relacionamentos'] != ""): ?>
	<?php unset($this->_sections['indice']);
$this->_sections['indice']['name'] = 'indice';
$this->_sections['indice']['loop'] = is_array($_loop=$this->_tpl_vars['relacionamentos']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['indice']['show'] = true;
$this->_sections['indice']['max'] = $this->_sections['indice']['loop'];
$this->_sections['indice']['step'] = 1;
$this->_sections['indice']['start'] = $this->_sections['indice']['step'] > 0 ? 0 : $this->_sections['indice']['loop']-1;
if ($this->_sections['indice']['show']) {
    $this->_sections['indice']['total'] = $this->_sections['indice']['loop'];
    if ($this->_sections['indice']['total'] == 0)
        $this->_sections['indice']['show'] = false;
} else
    $this->_sections['indice']['total'] = 0;
if ($this->_sections['indice']['show']):

            for ($this->_sections['indice']['index'] = $this->_sections['indice']['start'], $this->_sections['indice']['iteration'] = 1;
                 $this->_sections['indice']['iteration'] <= $this->_sections['indice']['total'];
                 $this->_sections['indice']['index'] += $this->_sections['indice']['step'], $this->_sections['indice']['iteration']++):
$this->_sections['indice']['rownum'] = $this->_sections['indice']['iteration'];
$this->_sections['indice']['index_prev'] = $this->_sections['indice']['index'] - $this->_sections['indice']['step'];
$this->_sections['indice']['index_next'] = $this->_sections['indice']['index'] + $this->_sections['indice']['step'];
$this->_sections['indice']['first']      = ($this->_sections['indice']['iteration'] == 1);
$this->_sections['indice']['last']       = ($this->_sections['indice']['iteration'] == $this->_sections['indice']['total']);
?>
	<table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr  bgcolor="<?php echo smarty_function_cycle(array('values' => "#eeeeee,#ffffff"), $this);?>
" class="texto">
        <td><em><?php echo $this->_tpl_vars['relacionamentos'][$this->_sections['indice']['index']]['tabela1']; ?>
 </em>( <strong><?php echo $this->_tpl_vars['relacionamentos'][$this->_sections['indice']['index']]['campo1']; ?>
</strong> ) is a foreign key of <em> <?php echo $this->_tpl_vars['relacionamentos'][$this->_sections['indice']['index']]['tabela2']; ?>
</em> ( <strong><?php echo $this->_tpl_vars['relacionamentos'][$this->_sections['indice']['index']]['campo2']; ?>
</strong> ) <a href="javascript:void(0);" onclick="excluirRelacionamento('<?php echo $this->_tpl_vars['relacionamentos'][$this->_sections['indice']['index']]['indice']; ?>
')"><img src="system_include/imgs/excluir.gif" width="16" height="16" hspace="0" vspace="0" border="0" /></a></td>
      </tr>
    </table>
	<?php endfor; endif; ?>
	<?php endif; ?>
    <form id="form1" name="form1" method="post" action="">
      <fieldset>
	  <legend>Build a Relationship </legend>
	  <br />
	  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
        <tr>
          <td ><h3 align="left" style="margin-left:10px;">Main table </h3></td>
          <td ><h3 align="left" style="margin-left:10px;">Child table </h3></td>
        </tr>
        <tr>
          <td ><select name="tabela1" id="tabela1" onchange="listaCampos(this.value,'campo1');">
		  <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['valores_tabelas'],'selected' => $this->_tpl_vars['selecionado_tabelas'],'output' => $this->_tpl_vars['valores_tabelas']), $this);?>

          </select>          </td>
          <td ><select name="tabela2" id="tabela2" onchange="listaCampos(this.value,'campo2');">>
		  <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['valores_tabelas'],'selected' => $this->_tpl_vars['selecionado_tabelas'],'output' => $this->_tpl_vars['valores_tabelas']), $this);?>

                    </select></td>
        </tr>
        <tr>
          <td ><select name="campo1" size="10" id="campo1" style="width:80%">
          </select>          </td>
          <td ><select name="campo2" size="10" id="campo2" style="width:80%">
                                        </select></td>
        </tr>
        <tr>
          <td >&nbsp;</td>
          <td >&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" >
            <div align="right">
              <input type="submit" name="Submit" value="Save" />
              </div></td></tr>
      </table>
	  </fieldset>
    </form>
  <br>
  <?php endif; ?>
  <?php if ($this->_tpl_vars['aviso'] != ""): ?>
   <h2 style="color:#FF0000"> <?php echo $this->_tpl_vars['aviso']; ?>
 </h2>
  <?php endif; ?>
  </div>
</div>
<!--end content -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--end navbar -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "rodape.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript">
<?php if ($this->_tpl_vars['erro'] == 'sim'): ?>
alert('Escolha um banco de dados');
document.location='escolherBanco.php';
<?php endif; ?>
</script>
</body>
</html>
