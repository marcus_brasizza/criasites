<?php /* Smarty version 2.6.19, created on 2011-12-08 15:14:10
         compiled from listaTabelas.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'listaTabelas.html', 25, false),)), $this); ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- DW6 -->
<head>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>:: Cria&ccedil;&atilde;o de tabelas ::</title>
<link rel="stylesheet" href="css/style.css" type="text/css" /></head>
<!-- The structure of this file is exactly the same as 2col_rightNav.html;
     the only difference between the two is the stylesheet they use -->
<body>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "topo.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- end masthead -->
<div id="content">
  <h2 id="pageName">Tables</h2>
  <div class="story">
    <p>Selected Database <strong> &quot;<?php echo $this->_tpl_vars['bancoSelecionado']; ?>
&quot;</strong></p>
    <table width="100%" border="0" cellpadding="2" cellspacing="3">
      <tr>
        <td width="*" bgcolor="#E2E2E2"><div align="left"><strong>Table Name </strong></div></td>
        <td width="10%" bgcolor="#E2E2E2"><div align="center"><strong>ORM Class </strong></div></td>
       <!-- <td width="10%" bgcolor="#E2E2E2"><div align="center"><strong>Form XML </strong></div></td> //-->
      </tr>
      <?php unset($this->_sections['indice']);
$this->_sections['indice']['name'] = 'indice';
$this->_sections['indice']['loop'] = is_array($_loop=$this->_tpl_vars['tabelas']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['indice']['show'] = true;
$this->_sections['indice']['max'] = $this->_sections['indice']['loop'];
$this->_sections['indice']['step'] = 1;
$this->_sections['indice']['start'] = $this->_sections['indice']['step'] > 0 ? 0 : $this->_sections['indice']['loop']-1;
if ($this->_sections['indice']['show']) {
    $this->_sections['indice']['total'] = $this->_sections['indice']['loop'];
    if ($this->_sections['indice']['total'] == 0)
        $this->_sections['indice']['show'] = false;
} else
    $this->_sections['indice']['total'] = 0;
if ($this->_sections['indice']['show']):

            for ($this->_sections['indice']['index'] = $this->_sections['indice']['start'], $this->_sections['indice']['iteration'] = 1;
                 $this->_sections['indice']['iteration'] <= $this->_sections['indice']['total'];
                 $this->_sections['indice']['index'] += $this->_sections['indice']['step'], $this->_sections['indice']['iteration']++):
$this->_sections['indice']['rownum'] = $this->_sections['indice']['iteration'];
$this->_sections['indice']['index_prev'] = $this->_sections['indice']['index'] - $this->_sections['indice']['step'];
$this->_sections['indice']['index_next'] = $this->_sections['indice']['index'] + $this->_sections['indice']['step'];
$this->_sections['indice']['first']      = ($this->_sections['indice']['iteration'] == 1);
$this->_sections['indice']['last']       = ($this->_sections['indice']['iteration'] == $this->_sections['indice']['total']);
?>
      <tr class="<?php echo smarty_function_cycle(array('values' => "linhapar,linhaimpar"), $this);?>
">
        <td><div align="left" style="padding-left:10px;"><?php echo $this->_tpl_vars['tabelas'][$this->_sections['indice']['index']]['tabela']; ?>
</div></td>
        <td><div align="center"><?php echo $this->_tpl_vars['tabelas'][$this->_sections['indice']['index']]['class']; ?>
</div></td>
        <!--<td><div align="center"><?php echo $this->_tpl_vars['tabelas'][$this->_sections['indice']['index']]['form']; ?>
</div></td> //-->
      </tr>
      <?php endfor; endif; ?>
    </table>
    <p>&nbsp;</p>
  </div>
</div>
<!--end content -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--end navbar -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "rodape.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript">
<?php if ($this->_tpl_vars['erro'] == 'sim'): ?>
alert('Escolha um banco de dados');
document.location='escolherBanco.php';
<?php endif; ?>
</script>
<br />
</body>
</html>
