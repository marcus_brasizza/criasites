<?
require("Smarty/Smarty.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");
require("classMaker/generateClass.class.php");
if(!isset($_SESSION['system']))
{
header('location:index.php');
}
$hostServer = $_SESSION['system']['host'];
$userServer = $_SESSION['system']['user'];
$passServer = $_SESSION['system']['pass'];
$path_projeto = $_SESSION['system']['path'].$_SESSION['system']['projeto'];
$connLocadora = Connection::UseDatabase($tipoBanco,$hostServer,$userServer,$passServer,$_SESSION['system']['database'],0,false);

$tpl = new Smarty();
$sys = new System();
$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
if(isset($_GET['classGen']))
{
 if($objGen = new classGenerate($hostServer,$userServer,$passServer,$_SESSION['system']['database'],$tipoBanco,$path_projeto));

 echo "<script language=\"Javascript\">";
 echo "alert(\"ORM Classes Created Sucessfully\\n  \\n".$_SESSION['system']['wwwSite'].$_SESSION['system']['projeto']."\");";
 echo "document.location='mostraClasses.php'";
 echo "</script>";
}



if(!isset($_SESSION['system']['database']))
{
	$tpl->assign('erro','sim');
	$tpl->assign('bancoSelecionado','Nenhum');
	$tpl->display('listaTabelas.html');
}
else
{
	$bancoSelecionado = $_SESSION['system']['database'];
}


$lista = $sys->listaTabelas($connLocadora,$path_projeto);

$tpl->assign('tabelas',$lista);
$tpl->assign('bancoSelecionado',$bancoSelecionado);
$tpl->display('listaTabelas.html');


?>