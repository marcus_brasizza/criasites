<?
require("Smarty/Smarty.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");
if(!isset($_SESSION['system']))
{
header('location:index.php');
}
$sys = new System();
$projeto = $_SESSION['system']['projeto'];
$path_projeto = $_SESSION['system']['path'].$_SESSION['system']['projeto'];
$bancoSelecionado = $_SESSION['system']['database'];
if(!isset($_SESSION['relacionamentos']))
{
	if(file_exists($path_projeto.'/relations/relations_'.$bancoSelecionado.'.xml'))
	{
		$xml = new SimpleXMLElement(file_get_contents($path_projeto.'/relations/relations_'.$bancoSelecionado.'.xml'));
		$total_geral = count($xml->tabela1);
		for($i=0;$i!=$total_geral;$i++)
		{
			$tabela1 =  "".$xml->tabela1[$i];
			$tabela2 =  "".$xml->tabela2[$i];
			$campo1 =  "".$xml->campo1[$i];
			$campo2 = "".$xml->campo2[$i];
			$_SESSION['relacionamentos'][$i]['tabela1'] = $tabela1;
			$_SESSION['relacionamentos'][$i]['tabela2'] = $tabela2;
			$_SESSION['relacionamentos'][$i]['campo1'] = $campo1;
			$_SESSION['relacionamentos'][$i]['campo2'] = $campo2;
			$_SESSION['relacionamentos'][$i]['indice'] = $i;
		}
	}
	else
	{
		session_register('relacionamentos');
	}
}

if(isset($_GET['excRel']))
{
 $ultimo = count($_SESSION['relacionamentos']);
 $total = count($_SESSION['relacionamentos']);
 if($ultimo != $_GET['excRel'])
 {
  for($i=$_GET['excRel'];$i<=$ultimo-1;$i++)
  {

	$_SESSION['relacionamentos'][$i]['tabela1'] = @$_SESSION['relacionamentos'][$i+1]['tabela1'];
	$_SESSION['relacionamentos'][$i]['tabela2'] =  @$_SESSION['relacionamentos'][$i+1]['tabela2'];
	$_SESSION['relacionamentos'][$i]['campo1'] =  @$_SESSION['relacionamentos'][$i+1]['campo1'];
	$_SESSION['relacionamentos'][$i]['campo2'] =  @$_SESSION['relacionamentos'][$i+1]['campo2'];
	$_SESSION['relacionamentos'][$i]['indice'] =  $i;
  }
  	unset($_SESSION['relacionamentos'][$i-1]);
    if ($sys->criaXmlRelacao($path_projeto,$total,$projeto,$bancoSelecionado))
    {
		echo "<script language=\"Javascript\">";
		echo "document.location='criarRelacionamentos.php'";
		echo "</script>";
    }
 }
 else
 {
 	unset($_SESSION['relacionamentos'][$_GET['excRel']]);
 }
}
$tpl = new Smarty();

$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
$hostServer = $_SESSION['system']['host'];
$userServer = $_SESSION['system']['user'];
$passServer = $_SESSION['system']['pass'];
if(!isset($_SESSION['system']['database']))
{
	$tpl->assign('erro','sim');
	$tpl->assign('bancoSelecionado','Nenhum');
	$tpl->display('criarRelacionamentos.html');
}
else
{
	$conn = Connection::UseDatabase($tipoBanco,$hostServer,$userServer,$passServer,$_SESSION['system']['database'],0,false);
	$escolha[] = 'Tables';
	$tabelas  = array_merge($escolha,$sys->listaTabelasCombo($conn,$bancoSelecionado));
	if(count($tabelas)<= 1){
		$tpl->assign('listar','nao');
		$tpl->assign('aviso','No tables found');
	}
	else{
		$tpl->assign('listar','sim');
		$tpl->assign('valores_tabelas',$tabelas);
	}

}
$tpl->assign('bancoSelecionado',$bancoSelecionado);

if($_POST == true)
{
	$total = count($_SESSION['relacionamentos']);
	$_SESSION['relacionamentos'][$total]['tabela1'] = $_POST['tabela1'];
	$_SESSION['relacionamentos'][$total]['tabela2'] = $_POST['tabela2'];
	$_SESSION['relacionamentos'][$total]['campo1'] = $_POST['campo1'];
	$_SESSION['relacionamentos'][$total]['campo2'] = $_POST['campo2'];
	$_SESSION['relacionamentos'][$total]['indice'] = $total;
    if ($sys->criaXmlRelacao($path_projeto,$total,$projeto,$bancoSelecionado))
    {
		echo "<script language=\"Javascript\">";
		echo "document.location='criarRelacionamentos.php'";
		echo "</script>";
    }
}

$tpl->assign('relacionamentos',$_SESSION['relacionamentos']);
$tpl->display('criarRelacionamentos.html');
?>