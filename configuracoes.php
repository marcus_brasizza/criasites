<?
require("Smarty/Smarty.class.php");
require("formMaker/gerador.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");


$tpl = new Smarty();
$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
// Instancia o objeto do sistema
$system = new System();
/* Checa se ja submeteu a p�gina */
if($_POST == true )
{
	$_SESSION['system']['host'] = $_POST['host'];
	$_SESSION['system']['user'] = $_POST['username'];
	$_SESSION['system']['pass'] = $_POST['password'];
	$_SESSION['system']['path'] = $_POST['path'];
	$_SESSION['system']['wwwSite'] = $_POST['wwwSite'];
	$_SESSION['system']['projeto'] = $_POST['projeto'];
	$tpl->assign('sucesso','sim');
	unset($_SESSION['relacionamentos']);
}
// Verifica se algum banco ja foi selecionado
$selecao = isset($_SESSION['system']['database'])?$_SESSION['system']['database']:'-1';
$host = isset($_SESSION['system']['host'])?$_SESSION['system']['host']:'';
$user = isset($_SESSION['system']['user'])?$_SESSION['system']['user']:'';
$pass = isset($_SESSION['system']['pass'])?$_SESSION['system']['pass']:'';
$path = isset($_SESSION['system']['path'])?$_SESSION['system']['path']:$_SERVER['DOCUMENT_ROOT'].'/';
$wwwSite = isset($_SESSION['system']['wwwSite'])?$_SESSION['system']['wwwSite']:'http://'.$_SERVER['SERVER_NAME'].'/';
$projeto = isset($_SESSION['system']['projeto'])?$_SESSION['system']['projeto']:'';
/* Pega os bancos no servidor */
/* Gera o formulario com os bancos de dados */
$gerador = new form('MyForm1','Initialization','','post','');
$gerador->SET_inputText(('Hostname'),'host',20,$host,$selecao,'');
$gerador->SET_inputText(('Username'),'username',20,$user,$selecao,'');
$gerador->SET_inputText(('Password'),'password',20,$pass,$selecao,'');
$gerador->SET_inputText(('OS Path'),'path',50,$path,$selecao,false);
$gerador->SET_inputText(('URL Path'),'wwwSite',50,$wwwSite,$selecao,false);
$gerador->SET_inputText(('Project Name'),'projeto',30,$projeto,$selecao,'');
$gerador->SET_submit('submit','Submit');
$gerador->form_end();
/* Fim do formul�rio */
/* Cria o formul�rio em uma vari�vel */
$formulario = $gerador->showForm();

$tpl->assign('areaForm',$formulario);
$tpl->display('configuracoes.html');


?>