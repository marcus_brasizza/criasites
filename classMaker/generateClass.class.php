<?
class classGenerate
{
	function __construct($host,$user,$pass,$db,$dataType,$pasta)
	{
		$cria_config = "<?php ".PHP_EOL."define(\"HOST\",\"$host\") ;".PHP_EOL.
                               "define(\"USER\",\"$user\") ;".PHP_EOL.
                               "define(\"PASS\",\"$pass\") ;".PHP_EOL.
                               "define(\"DATABASE\",\"$db\") ;".PHP_EOL.
                               "define(\"ENGINE\",\"$dataType\") ;".PHP_EOL. " ?>";





                /* Criar variaveis do banco de dados */
		if(file_exists($pasta.'/config/db.php'))
		{
			@unlink($pasta.'/config/db.php');
		}
		$handle = fopen($pasta.'/config/db.php','a');
		fwrite($handle,$cria_config);
		fclose($handle);

		/* Criar as Classes */
		$mysql = Connection::UseDatabase($dataType,$host,$user,$pass,$bd,0,false);
		$mysql2 = Connection::UseDatabase($dataType,$host,$user,$pass,$bd,0,false);
		$mysql->select("Show tables");
		/* Procura no XML se existe algum relacionamento para essa tabela */
		if(file_exists($pasta.'/relations/relations_'.$bd.'.xml')){
                    $xml = simplexml_load_file($pasta.'/relations/relations_'.$bd.'.xml');
		}
		/* Fim da procura */
		$totalTabela1 = count($xml->tabela1);
		$propObj = array();
		$propthis = array();
		$propref = array();
		for($i=0;$i!=$mysql->numRecords();$i++)
		{
			$tabelas[] = $mysql->getLine(0);
			$mysql->next();
		}
		if(count($tabelas) > 0 )
		{
			$utils = array();
			foreach($tabelas as $xtabs)
			{

				$t = explode('_',$xtabs);
				if(count($t) > 0){
					foreach($t as $tt){
						$tabs.= ucfirst($tt);
					}
				}

				$fields = "/* Objects from tables */".PHP_EOL;

				for($j=0 ; $j !=  $totalTabela1 ; $j++){
					if($xml->tabela1[$j] == $xtabs){
						$propObj[] = "".$xml->tabela2[$j]."";
						$propthis[] = "".$xml->campo1[$j]."";
						$propref[] = "".$xml->campo2[$j]."";
						if(!in_array($xml->tabela2[$j],$utils)){
							$fields .= " \t\tprivate $"."".$xml->tabela2[$j]."".";".PHP_EOL;

						}
						$utils[] = $xml->tabela2[$j];
					}

				}
				$listAll = $this->createListAll($xtabs,$propObj,$propthis,$propref);
				$fields .= "\t/* end Objects from tables */".PHP_EOL;
				$fields .= "\t/* Properties */".PHP_EOL;
				//$fields.= " \tprivate \$conn; // Conection instance".PHP_EOL;
				$mysql->select("Show fields from $xtabs");
				$mysql2->select("Show fields from $xtabs");
				$this->createDefs($mysql2,$xml,$xtabs,$pasta);
				for($k=0;$k!=$mysql->numRecords();$k++)
				{
					if($mysql->getLine(5) == 'auto_increment'){
						//$fields .= " \tprotected \$keyField = \"".$mysql->getLine(0)."\"; /* Key Field */".PHP_EOL;
					}
					$fields .= " \tprivate $".$mysql->getLine(0).";".PHP_EOL;
					$properties[] = $mysql->getLine(0);

					$mysql->next();
				}


                                            $comment = "\t /**
                            * $xtabs class
                            * This class manipulates the table $xtabs
                            * @requires    >= PHP 5
                            * @author      Marcus Brasizza            <mvbrasizza@oztechnology.com.br>
                            * @copyright   (C)".date('Y')."
                            * @DataBase = $bd
                            * @DatabaseType = $dataType
                            * @Host = $host
                            * @date ".date('d/m/Y')."
                            **/ ";
				$getters = $this->createGets($properties);
				$getters.= $this->createGetsObj($propObj,$propthis,$propref);
				$setters = $this->createSets($properties);

				$padrao = file_get_contents(ROOT_FOLDER.'classMaker/model/model.class_core.php');
				$extended = file_get_contents(ROOT_FOLDER.'classMaker/model/model.class.php');
                                $controller = file_get_contents(ROOT_FOLDER.'classMaker/model/controller.class.php');
				$padrao = str_replace('#[#GETTERS#]#',$getters,$padrao);
				$padrao = str_replace('#[#SETTERS#]#',$setters,$padrao);
				$padrao = str_replace('#[#NameClass#]#',($tabs),$padrao);
				$padrao = str_replace('#[#comments#]#',$comment,$padrao);
				$padrao =str_replace('#[#Vars#]#',trim($fields),$padrao);

				$extended=str_replace('#[#DECISAOLISTALL#]#',trim($listAll),$extended);
				$extended = str_replace('#[#NameClass#]#',($tabs),$extended);
				$extended = str_replace('#[#comments#]#',$comment,$extended);



                                $controller = str_replace('#[#NameClass#]#',($tabs),$controller);
				//$areas = array();
				if(file_exists($pasta.'/app/bean/'.($tabs).'Bean.php'))
				{
					//$handle = fopen($pasta.'/bean/core_'.$tabs.'.class.php','r');

					//$file = fread($handle,filesize($pasta.'/bean/core_'.$tabs.'.class.php'));
					//fclose($handle);

					//for($i=0;$i!=$this->custom;$i++)
					//{
					//preg_match("/<custom".($i+1).">((?:(?!<\/custom".($i+1).">).)*)<\/custom".($i+1).">/is", $file, $return);
					//$area = str_replace(array('/*','*/'),'',$return[0]);
					//$area = str_replace(array('<custom'.($i+1).'>','</custom'.($i+1).'>'),'',$area);
					//$areas[] = trim($area);
					//}
					@unlink($pasta.'/app/bean/'.($tabs).'Bean.php');

					//@unlink($pasta.'/documents/'.($tabs).'Bean.phps');
				}
				/*else
				{
				for($i=0;$i!=$this->custom;$i++)
				{
				$areas[] = '';
				}
				}*/
				//$handle = fopen($pasta.'/app/bean/'.($tabs).'Bean.php','a');
				//fwrite($handle,$padrao);
				//fclose($handle);
                                file_put_contents($pasta.'/app/bean/'.($tabs).'Bean.php', $padrao);
				if(file_exists($pasta.'/app/model/'.($tabs).'Model.php')){

                                }else{
					$handle = fopen($pasta.'/app/model/'.($tabs).'Model.php','a');
					fwrite($handle,$extended);
					fclose($handle);
                                }


                                if(file_exists($pasta.'/app/controller/'.($tabs).'Controller.php')){

                                }else{
					$handle = fopen($pasta.'/app/controller/'.($tabs).'Controller.php','a');
					fwrite($handle,$controller);
					fclose($handle);
                                }

				/* Document Data */
				//$handle = fopen($pasta.'/documents/'.($tabs).'Bean.phps','a');
				//fwrite($handle,$padrao);
				//fclose($handle);
				/* Put the custom data again in the class */
				/* if(count($areas) > 0 ){
				$this->populateCustom($pasta.'/class/'.$tabs.'.class.php',$areas);
				$this->populateCustom($pasta.'/documents/'.$tabs.'.class.phps',$areas);
				}*/
                                if(!is_dir($pasta.'/app/view/mod_'.$tabs)){
                                @mkdir($pasta.'/app/view/mod_'.$tabs.'/languages',0777,true);
                                @mkdir($pasta.'/app/view/mod_'.$tabs.'/template',0777,true);
                                @mkdir($pasta.'/app/view/mod_'.$tabs.'/template_c',0777,true);
                                }

				unset($fields);
				unset($properties);
				$propObj = array();
				$propthis = array();
				$propref = array();
				$tabs = '';
			}
		}
		//@copy('system_include/DatabaseClass.php',$pasta.'/core/database.class.php');
		//@copy('system_include/DatabaseClass.php',$pasta.'/documents/database.class.phps');

		$old = @umask(0);
		if(!file_exists($pasta.'/index.php')){
		@copy('system_include/index.php',$pasta.'/index.php');
	}
								@copy('system_include/commons.class.php',$pasta.'/system/commons.class.php');
                @copy('system_include/application/config.php',$pasta.'/config/config.php');
                @copy('system_include/application/settings.php',$pasta.'/config/settings.php');
                @copy('system_include/application/require.php',$pasta.'/config/require.php');
                @copy('system_include/application/define.php',$pasta.'/config/define.php');
                @copy('system_include/application/autoload.php',$pasta.'/config/autoload.php');
                @copy('system_include/application/system/Controller.php',$pasta.'/system/Controller.php');
                @copy('system_include/application/system/System.php',$pasta.'/system/System.php');
                @copy('system_include/application/system/Model.php',$pasta.'/system/Model.php');




		@chmod($pasta.'/index.php',0777);
		@umask($old);
		return true;
	}

	function createGetsObj($propObj=array(),$propthis,$propref)
	{
		$getters = "";
		$totalProp = count($propObj);
		if(count($totalProp) > 0 )
		{
			$getters.= "\t/* GET OBJECTS */".PHP_EOL.PHP_EOL."\t";
			for ( $i = 0 ; $i != $totalProp ; $i++)
			{
				$getters.= "\tfunction get".ucFirst($propObj[$i])."(){".PHP_EOL;
				//$getters.= "\t\trequire_once(\"class/$propObj[$i].class.php\");".PHP_EOL;
				$getters.= "\t\t\t\$myObj = new $propObj[$i](\$this->conn,\$this->__get('$propthis[$i]'));".PHP_EOL;
				$getters.= "\t\t\t\$this->$propObj[$i] = \$myObj;".PHP_EOL;
				$getters.= "\t\t\treturn \$myObj;".PHP_EOL;
				$getters.= "\t\t}"."/* End of get object $propObj[$i] */".PHP_EOL.PHP_EOL."\t";
			}
			$getters.= "/* END GET OBJECTS */".PHP_EOL."\t";
		}
		return $getters;
	}

	function createGets($fields,$propObj=array())
	{
		$getters = "";
		if(count($fields) > 0 )
		{
			$getters.= "/* GET FIELDS FROM TABLE */\t".PHP_EOL.PHP_EOL;
			foreach ($fields as $field)
			{
				$getters.="\t\t".'/**
			 * @category Recuperar dados da propriedade '.$field.'
			 * Metodo para recuperar o valor de '.$field.'
			 * $dado = $MyClass->get'.ucfirst($field).'()
			 * '.PHP_EOL.'
			 */'.PHP_EOL;
				$getters.= "\t\tfunction get".ucfirst($field)."(){".PHP_EOL;
				$getters.= "\t\t\treturn \$this->$field;".PHP_EOL;
				$getters.= "\t\t}"."/* End of get $field */".PHP_EOL.PHP_EOL;
			}
		}
		return $getters;
	}

	function createSets($fields)
	{
		$setters = "";
		if(count($fields) > 0 )
		{
			$setters.= "/* SET FIELDS FROM TABLE */\t".PHP_EOL.PHP_EOL;
			foreach ($fields as $field)
			{
				$setters.="\t\t".'/**
			 * @category Inserir dados na propriedade '.$field.'
			 * Metodo inserir o valor de '.$field.'
			 * $MyClass->set'.ucfirst($field).'($valor)
			 * '.PHP_EOL.'
			 */'.PHP_EOL;
				$setters.= "\t\tfunction set".ucfirst($field)."(\$value){".PHP_EOL;
				$setters.= "\t\t\$this->$field = \$value;".PHP_EOL;
				$setters.= "\t\t}"."/* End of SET $field */".PHP_EOL.PHP_EOL;
			}
		}
		return $setters;
	}

	function populateCustom($file,$array)
	{
		$handle = fopen($file,'r');
		$data = fread($handle,filesize($file));
		fclose($handle);
		@unlink($file);
		for($i = 0 ; $i!=count($array);$i++)
		{
			if($array[$i] == "#[#CUSTOM".($i+1)."#]#")
			{
				$data = str_replace("#[#CUSTOM".($i+1)."#]#",'',$data);
			}
			elseif(empty($array[$i]))
			{
				if($i == 0){

					$dt = "if(\$key == NULL){ \$key = \$this->keyField ;}".PHP_EOL;
					$dt .= "\t\t\$return = parent::__save(\$this,\$campos,\$key);";
					$data = str_replace("#[#CUSTOM".($i+1)."#]#",$dt,$data);
				}
				elseif($i == 1 ){
					$dt = "if(\$key == NULL){ \$key = \$this->keyField ;}".PHP_EOL;
					$dt.="\t\t\$return =  parent::__delete(\$this,\$value,\$key);";
					$data = str_replace("#[#CUSTOM".($i+1)."#]#",$dt,$data);
				}
			}
			else
			{
				$data = str_replace("#[#CUSTOM".($i+1)."#]#",$array[$i],$data);
			}
		}
		$handle = fopen($file,'a');
		fwrite($handle,$data);
		fclose($handle);
		return true;
	}

	/**/
	function createListAll($tabs,$propObj,$propthis,$propref)
	{
		if(count($propObj) <=0){
			return '$arrLista =  parent::__listAll($conn, $filter,$order,$class);';
		}else{
			$ret = '
			if(isset($filter) && $filter!=""){
			$filter = " WHERE ".$filter;
			}
			if(isset($order) && $order!=""){
				$order = " ORDER BY ".$order." ASC ";
			}
			$commons = new Commons();
			$conn->select("Show fields from '.$tabs.'");
			for($i=0;$i!=$conn->numRecords();$i++)
			{
				$fields[] = $conn->getLine(0);
				$conn->next();
			}';
			foreach($propObj as $classes){
				$ret.=PHP_EOL."\t\t".'$conn->select("Show fields from '.$classes.'");
			for($i=0;$i!=$conn->numRecords();$i++)
			{
				$fields[] = $conn->getLine(0);
				$conn->next();
			}';
			}
			$ret.=PHP_EOL."\t\t".'$conn->select("SELECT * FROM '.$tabs.' ';
			for($i=0;$i!=count($propObj);$i++){
				$ret.= 'LEFT JOIN '.$propObj[$i].' ON '.$propObj[$i].'.'.$propref[$i].' = '.$tabs.'.'.$propthis[$i].PHP_EOL."\t\t";
			}
			$ret.='".$filter." ".$order);

			for($i=0,$arrLista = NULL;$i!=$conn->numRecords();$i++)
			{
				$arrLista[$i] = new stdClass();
				foreach($fields as $campos)
				{
					$arrLista[$i]->$campos = $commons->convert2View($campos,$conn->getLine($campos));
				}
				$conn->next();
			}';
			return $ret;
		}
	}

	function createDefs($mysql2,$xml,$tabs,$pasta){
                /*
		$propObj = array();
		$propthis = array();
		$propref = array();

		for($j=0 ; $j !=  count($xml->tabela1) ; $j++){
			if(isset($xml->tabela1[$j])){
				if($xml->tabela1[$j] == $tabs){

					$propObj[] = "".$xml->tabela2[$j]."";
					$propthis[] = "".$xml->campo1[$j]."";
					$propref[] = "".$xml->campo2[$j]."";
				}
			}
		}
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?>'.PHP_EOL;
		$xmlString .= '<fields>'.PHP_EOL.PHP_EOL;
		for($i=0;$i!= $mysql2->numRecords();$i++){
			if(($mysql2->getLine(3) != 'PRI') && ($mysql2->getLine(5) != 'auto_increment' )){
				$xmlString .= '<field>'.PHP_EOL;
				$xmlString .= "\t".'<label>'.ucfirst($mysql2->getLine(0)).':</label>'.PHP_EOL;
				$xmlString .= "\t".'<name>'.($mysql2->getLine(0) ).'</name>'.PHP_EOL;
				$xmlString .= "\t".'<type>'.($mysql2->getLine(1)).'</type>'.PHP_EOL;
				$xmlString .= "\t".'<req>'.($mysql2->getLine(2) ).'</req>'.PHP_EOL;
				$xmlString .= "\t".'<key>'.($mysql2->getLine(3)) .'</key>'.PHP_EOL;
				$xmlString .= "\t".'<defValue>'.($mysql2->getLine(4)).'</defValue>'.PHP_EOL;
				$xmlString .= "\t".'<extra>'.($mysql2->getLine(5)).'</extra>'.PHP_EOL;


				for($ind = 0; $ind != count($propthis) ; $ind++){
					if(isset($propthis[$ind])){
						if($propthis[$ind] == $mysql2->getLine(0)){
							$xmlString .= "\t".'<referTo>'.$propObj[$ind].'</referTo>'.PHP_EOL;
							$xmlString .= "\t".'<fieldRefer>'.$propref[$ind].'</fieldRefer>'.PHP_EOL;
						}
					}
				}

				$xmlString .= '</field>'.PHP_EOL.PHP_EOL;
			}
			$mysql2->next();
		}



		$xmlString .= '</fields>'.PHP_EOL;

		@unlink("$pasta/forms/$tabs.form.xml");
		$handle = fopen("$pasta/forms/$tabs.form.xml","a");
		fwrite($handle,$xmlString);
		fclose($handle);*/

	}


}
