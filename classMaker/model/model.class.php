<?php
// $Id: model.class_core.php,v 1.1.1.1 2009/02/18 13:10:20 marcus Exp $
#[#comments#]#

class #[#NameClass#]#Model  extends Commons{

	/**
	 * @category Constructor
	 * Metodo que sera chamado logo que a classe for instanciada.
	 * Por padrao o KeyField sera a chave primaria da tabela
	 * @param DB Source $conn
	 * @param Value $id
	 * @param KeyField $key
	 */
    function __construct($conn,$id=NULL,$key=NULL)
    {

    }

	/**
	 * @category Save
	 * Generic method to save some data in the database
	 * By default the keyfield is going to be the primary key
	 * @param campos in array $campos
	 * @param KeyField $key
	 */
    function save($key=NULL)
   	{


	}

	/**
	 * @category Delete
	  * Generic method to remove some data in the database
	 * By default the keyfield is going to be the primary key
	 * @param campos in array $campos
	 * @param KeyField $key
	 */
	function delete($value,$key=NULL)
	{

	}

	/**
	 * @category One key list
	 * Generic method to list one key in the database
	 * @param Database instance $conn
	 * @param KeyField $key
	 * @param Order $order
	 * @param Filter $filter
	 */
	static function listSingle($conn,$key,$order ='',$filter ='',$class) {


	}

	/**
	 * @category List all fields
	 * Generic method to list all fields in the database, or with some relation with the main database
	 * @param Filter $filter
	 * @param Order $order
	 */
	static function listAll($conn,$filter="",$order ='',$class) {

	}

	/**
	 * @category List with pagination
	 * Generic method to build a navigation between pages with a paginated result.
	 * @param Filter $filter
	 * @param Order $order
	 */

	static function partialResult($conn,$filter="",$order ='',$atual=0,$rpp=10,$path,$class){


	}

}
?>
