<?
require("Smarty/Smarty.class.php");
require("system_include/config.php");
require("system_include/DatabaseClass.php");
require("system_include/system.class.php");
$tpl = new Smarty();
$tpl->template_dir = "tpl";
$tpl->compile_dir = "tpl_c";
$tpl->left_delimiter = "#[#";
$tpl->right_delimiter = "#]#";
$hostServer = $_SESSION['system']['host'];
$userServer = $_SESSION['system']['user'];
$passServer = $_SESSION['system']['pass'];
if(!isset($_SESSION['system']))
{
header('location:index.php');
}
$sys = new System();
$projeto = $_SESSION['system']['projeto'];
$path_projeto = $_SESSION['system']['path'].$_SESSION['system']['projeto'];
$path_projetowww = $_SESSION['system']['wwwSite'].$_SESSION['system']['projeto'];

$bancoSelecionado = $_SESSION['system']['database'];
$tpl->assign('bancoSelecionado',$bancoSelecionado);
$conn = Conexao::UsarBanco($tipoBanco,$hostServer,$userServer,$passServer,$_SESSION['system']['database'],0,false);
if(isset($_GET['tabelaSelecionada'])){
	$tpl->assign('nomeTabela',$_POST['tabela']);
	$xml = simplexml_load_file($path_projeto.'/forms/'.$_POST['tabela'].'.form.xml');
	$arr = array();
	$k =0 ;
	for($i=0;$i!= count($xml->field) ; $i++){
		foreach($xml->field[$i] as $ind=>$valor){
			$arr[$i][$ind] = "".$valor;
		}
	}
}
if(count($arr)>0){
	$tArr = count($arr);
	for($i=0;$i!=$tArr;$i++){
		if(isset($arr[$i]['referTo'])){
			include($path_projeto.'/class/'.$arr[$i]['referTo'].'.class.php');
			$cl = new $arr[$i]['referTo']($conn);
			$list = $cl->listAll();
			print_r($list);
			$idList = array();
			$valueList = array();
			if(count($list) > 0 ){
				for($j=0;$j!=count($list);$j++){
					if(count($list[$j])  > 0 ){
						$k = 0;
						foreach($list[$j] as $ind=>$values){
							if($k == 0){
								$idList[] = $values;
							}elseif($k== 2){
								$valueList[] = $values;
							}
							$k++;
						}
					}
					$arr[$i]['idList'] = $idList;
					$arr[$i]['valueList'] = $valueList;
				}
			}

		}
	}
}


$tpl->assign('campos',$arr);
$tpl->display('geraFormulario.html');
?>