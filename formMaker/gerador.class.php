<?php

class form {
	private $dataForm ;
	private $dateInputs;

	#Form Create:
	function __construct($name,$labelValue,$action,$method,$onsubmit='')
	 {
		$this->dataForm = "<form name=\"$name\" id=\"$name\" action=\"".addslashes($action)."\" method=\"$method\" onsubmit=\"".addslashes($onsubmit)."\" encrypt=\"multipart/form-data\">".PHP_EOL."<fieldset>".PHP_EOL."<legend>$labelValue</legend>".PHP_EOL;
	 }
	#Function: <input text>

	function SET_inputText($labelValue,$name,$size,$textValue='',$keypress = '', $readonly = false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }

		$this->dataForm.="<label for=\"$name\" >".utf8_decode($labelValue)."<br /></label>".PHP_EOL."<input type=\"text\" name=\"$name\" id=\"$name\" size=\"$size\" maxlength=\"$size\" value=\"$textValue\"  onkeypress=\"$keypress\" $read/><br />".PHP_EOL;
	}

	function SET_inputPassword($labelValue,$name,$size,$textValue='',$keypress = '', $readonly = false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }

		$this->dataForm.="<label for=\"$name\" >".utf8_decode($labelValue)."<br /></label>".PHP_EOL."<input type=\"password\" name=\"$name\" id=\"$name\" size=\"$size\" maxlength=\"$size\" value=\"$textValue\"  onkeypress=\"$keypress\" $read/><br />".PHP_EOL;
	}

	function SET_inputNumber($labelValue,$name,$size,$textValue='',$keypress = '', $readonly = false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }

		$this->dataForm.="<label for=\"$name\" >".htmlentities($labelValue)."<br /></label>".PHP_EOL."<input type=\"text\" name=\"$name\" id=\"$name\" size=\"$size\" maxlength=\"$size\" value=\"$textValue\"  onkeypress=\"mascara(this,soNumeros);$keypress\" $read/><br />".PHP_EOL;
	}

	function SET_DateInput($labelValue,$name,$size,$textValue='',$keypress = '',$readonly=false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }
		$this->dateInput[] = $name;
		$this->dataForm.="<label for=\"$name\" >".($labelValue)."<br /></label>".PHP_EOL."<input type=\"text\" name=\"$name\" id=\"$name\" size=\"$size\" maxlength=\"$size\" value=\"$textValue\" onKeypress=\"$keypress\" $read /><br />".PHP_EOL;
	}

	#Function: <selects>
	function SET_select($labelValue,$name,$size,$values,$selected='',$onchange='',$readonly=false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }
		$select = "<label for=\"$name\" >$labelValue<br /></label>".PHP_EOL;
		$select .= "<select name=\"".$name."\" id=\"".$name."\" size=\"$size\"  onchange=\"$onchange\"  $read>".PHP_EOL;
		$select.="<option value=\"-1\">- Escolha - </option>".PHP_EOL;
		if(is_array($values) > 0 )
		{
			foreach($values as $value=>$data)
			{
				if($value == $selected)
				{
					$sel = 'selected';
				}
				else
				{
					$sel = '';
				}
				$select.="<option value=\"$value\" $sel>$data</option>".PHP_EOL;
			}
		}
		$select.="</select><br />";
		$this->dataForm.=$select;
	}

	#Function: Label Alone
	function SET_label($labelValue)
	 {
		 $lbl ="<label ><br />$labelValue<br /><br /></label>".PHP_EOL;
		 $this->dataForm.=$lbl;
	 }
	#Function: <input checkbox...>
	function SET_checkbox($labelValue,$name,$checked='',$valueCheckbox='',$onclick='',$readonly=false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }
		 $chkbox = '';
		 $checked = ($checked) ? 'checked = "checked"' : '';
		 $chkbox.="<label for=\"$name\" >$labelValue</label>".PHP_EOL;
		 $chkbox.="<input type=\"checkbox\" name=\"$name\" id=\"$name\"  value=\"$valueCheckbox\"  onclick=\"$onclick\" $read $checked /><br />".PHP_EOL;
		 $this->dataForm.=$chkbox;
	}

	#Function: <input radio...>
	function SET_radio($labelValue,$name,$checked='',$valueCheckbox='',$onclick='',$readonly=false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }
		 $chkbox = '';
		 $checked = ($checked) ? 'checked = "checked"' : '';
		 $chkbox.="<label for=\"$name\" >$labelValue</label>".PHP_EOL;
		 $chkbox.="<input type=\"radio\" name=\"$name\" id=\"$name\"  value=\"$valueCheckbox\"  onclick=\"$onclick\" $read $checked /><br />".PHP_EOL;
		 $this->dataForm.=$chkbox;
	}
	#Function: <textarea...>
	function SET_textarea($labelValue,$name,$rows,$cols,$value='',$add='')
	{
		$txtarea = '';
		$txtarea.="<label for=\"$name\"  rows=\"$rows\" cols=\"$cols\"><br />$labelValue<br /></label>".PHP_EOL;
		$txtarea.="<textarea name=\"$name\" id=\"$name\">";
		$txtarea.=$value;
		$txtarea.="</textarea><br/>";
		 $this->dataForm .= $txtarea;
		//return "\t<span>".$name_txt.":<br /><textarea name=\"".$name."\" rows=\"".$rows."\" cols=\"".$cols."\">".$value."</textarea></span><br />\n";
	}
	#Function: <input file>
	function SET_file($labelValue,$name,$size,$textValue='',$keypress = '', $readonly = false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }

		$this->dataForm.="<label for=\"$name\" >".htmlentities($labelValue)."<br /></label>".PHP_EOL."<input type=\"file\" name=\"$name\" id=\"$name\" size=\"$size\" maxlength=\"$size\" value=\"$textValue\"  onkeypress=\"$keypress\"  $read/><br />".PHP_EOL;
	}
	#Function: <input hidden...>
	function SET_hidden($name,$textValue='')
	{
		$this->dataForm.="<input type=\"hidden\" name=\"$name\" id=\"$name\" value=\"$textValue\"/>".PHP_EOL;
	}
	#Function: Submit/Reset
	function SET_submit($name , $textValue='' ,  $readonly = false)
	{
		if($readonly)
		 {
		  $read = 'readonly = "readonly"';
		 }
		 else
		 {
		  $read = '';
		 }

		$this->dataForm.="<input type=\"submit\" name=\"$name\" id=\"$name\" value=\"$textValue\"    $read/><br />".PHP_EOL;
	}

	#Closing the form
	function form_end()
	{
		$this->dataForm .="</fieldset>".PHP_EOL."</form>".PHP_EOL;
	}
	#Show Form
	function showForm()
	{
	 return $this->dataForm;
	}
	#Create Date Mask
	function maskDateCreate()
	 {
	 	$scripts = '';
	 	if(isset($this->dateInput)){
	   if(is_array($this->dateInput))
	    {
		 foreach($this->dateInput as $inputs)
		  {
		   $scripts.="	jQuery(function($){
				$(\"#{$inputs}\").datepicker({showOn: 'button', buttonImage: 'imgs/calendar.gif', buttonImageOnly: true});
			});";
		  }
		}
		return $scripts;
	 }else{
	 	return '';
	 }
	 }
}
############################################# FIM DA CLASSE #############################################

?>